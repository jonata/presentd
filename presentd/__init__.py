#######################################################################
#
# Presentd
#
#######################################################################

__version__ = '22.06.01.02'

__appname__ = 'Presentd'
__domain__ = 'presentd.jonata.org'
__desktopid__ = 'org.jonata.Presentd'
# __appid__ = ''

__author__ = 'Jonatã Bolzan Loss'
__email__ = 'jonata@jonata.org'
__website__ = 'https://presentd.jonata.org'
__bugreport__ = 'https://gitlab.com/jonata/presentd/issues'

__ispypi__ = False
