import os
import sys
from PySide6.QtWidgets import QApplication, QWidget, QMainWindow, QGridLayout
from PySide6.QtGui import QIcon, QFontDatabase
from PySide6.QtCore import Qt, QTimer, QThreadPool

from presentd import translation, config
from presentd.modules import video, image
from presentd.interface import left_panel, playlist_panel, right_panel, monitor_window
from presentd.paths import PATH_PRESENTD_GRAPHICS, PATH_PRESENTD_USER_CONFIG_FILE, get_graphics_path

# if ACTUAL_OS == 'darwin':
#     from presentd.paths import NSURL


class Presentd(QMainWindow):
    """The main window (QWidget) class"""
    def __init__(self):
        super().__init__()
        self.setWindowTitle('Presentd')
        self.setWindowIcon(QIcon(os.path.join(PATH_PRESENTD_GRAPHICS, 'presentd.png')))
        self.setAcceptDrops(True)
        self.setWindowState(Qt.WindowMaximized)
        self.setMinimumSize(860, 450)

        self.threadpool = QThreadPool()
        self.threadpool.setMaxThreadCount(4)

        translation.load_translation_files()
        self.selected_language = 'en'
        translation.set_language(self.selected_language)

        QFontDatabase.addApplicationFont(os.path.join(PATH_PRESENTD_GRAPHICS, 'Ubuntu-RI.ttf'))
        QFontDatabase.addApplicationFont(os.path.join(PATH_PRESENTD_GRAPHICS, 'Ubuntu-R.ttf'))
        QFontDatabase.addApplicationFont(os.path.join(PATH_PRESENTD_GRAPHICS, 'Ubuntu-B.ttf'))
        QFontDatabase.addApplicationFont(os.path.join(PATH_PRESENTD_GRAPHICS, 'Ubuntu-BI.ttf'))

        self.setStyleSheet(open(os.path.join(PATH_PRESENTD_GRAPHICS, 'light.qss')).read().replace('PATH_PRESENTD_GRAPHICS/', get_graphics_path('_')[:-1]))

        monitor_window.load(self)

        self.main_widget = QWidget()
        self.setCentralWidget(self.main_widget)
        self.main_widget.setLayout(QGridLayout())
        self.main_widget.layout().setContentsMargins(0, 0, 0, 0)
        self.main_widget.layout().setSpacing(0)

        left_panel.load(self)
        playlist_panel.load(self)
        right_panel.load(self)

        playlist_panel.update_widgets(self)

        self.settings = config.load(PATH_PRESENTD_USER_CONFIG_FILE)

        video.load(self)
        image.load(self)

        self.left_panel_widget.vertical_menu.layout().addStretch()

        self.monitor_window.show()

        self.timer = QTimer(self)
        self.timer.setInterval(int(1000 / self.settings.get('interface_framerate', 12)))
        self.timer.timeout.connect(lambda: update_things(self))
        self.timer.start()

        translate_widgets(self)

    def closeEvent(self, event):
        config.save(self.settings, PATH_PRESENTD_USER_CONFIG_FILE)
        self.monitor_window.close()
        event.accept()


def translate_widgets(self):
    left_panel.translate(self)
    right_panel.translate(self)

    video.translate(self)
    image.translate(self)


def update_things(self):
    self.right_panel_monitor.main_background_qimage = self.monitor_window.grab().toImage()
    self.right_panel_monitor.update()
    # self.right_panel_monitor.setPixmap(self.monitor_window.grab())  # ), self.monitor_window.x(), self.monitor_window.y(), self.monitor_window.width(), self.monitor_window.height()))
    # self.right_panel_monitor_player_timeline.update()

# def resizeEvent(self, event):
#     self.background_label.setGeometry(0, 0, self.width(), self.height())
#     self.background_watermark_label.setGeometry(int((self.width() * .25) - 129), int((self.height() * .5) - 129), 258, 258)
#     right_panel.resize(self)
#     playlist_panel.resize(self)
#     left_panel.resize(self)
#     options_box.resize(self)
#     event.accept()

# def keyPressEvent(self, event):
#     None


def main():
    app = QApplication(sys.argv)
    app.setApplicationName("Presentd")

    import locale
    locale.setlocale(locale.LC_NUMERIC, 'C')

    app.main = Presentd()
    app.main.show()

    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
