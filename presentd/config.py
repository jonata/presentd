"""Config functions. Load and save files.

"""

import os
import json

from presentd.paths import PATH_PRESENTD_USER_CONFIG_FILE


def load(config_file_path=False):
    """Config load function. Provide a file path with 'config_file_path'.
    It will return a dict with the settings.
    """
    config = {}
    if config_file_path and os.path.isfile(config_file_path):
        with open(config_file_path) as fileobj:
            config = json.load(fileobj)

    if not config.get('recent_media_files', False):
        config['recent_media_files'] = {}

    if not config['recent_media_files'].get('videos', False):
        config['recent_media_files']['videos'] = []

    # if not config.get('shortcuts', False):
    #     config['shortcuts'] = {}

    # if not config.get('safety_margins', False):
    #     config['safety_margins'] = {}

    # if not config.get('autosave', False):
    #     config['autosave'] = {}

    # if not config.get('timeline', False):
    #     config['timeline'] = {}

    return config


def save(config=False, config_file_path=False):
    """Config save function. Provide a dict and
    a file path with 'config_file_path'.
    """
    if config:
        if not config_file_path:
            config_file_path = os.path.join(PATH_PRESENTD_USER_CONFIG_FILE)
        with open(config_file_path, 'w') as fileobj:
            json.dump(config, fileobj)
