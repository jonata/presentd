from PySide6.QtWidgets import QVBoxLayout, QWidget, QPushButton, QLabel, QHBoxLayout, QStackedWidget, QSizePolicy
from PySide6.QtCore import Qt, QPropertyAnimation, QEasingCurve

from presentd.utils import generate_effect
from presentd.translation import _


class left_panel_addition_menu_button(QPushButton):
    def __init__(widget):
        super().__init__()
        widget.setProperty('class', 'left_panel_addition_menu_button')
        widget.setCheckable(True)


def load(self):
    class left_panel_widget(QWidget):
        def __init__(widget):
            super().__init__()
            widget.setObjectName('left_panel_widget')
            widget.setProperty('is_hidden', True)
            widget.selected_addition = {}

            widget.additions_panel = QWidget(parent=widget)
            widget.additions_panel.setObjectName('left_panel_widget_additions_panel')
            widget.additions_panel.setProperty('size_x_when_hidden', 70)
            widget.additions_panel.setLayout(QHBoxLayout())
            widget.additions_panel.layout().setContentsMargins(20, 69, 10, 20)
            widget.additions_panel.layout().setSpacing(5)
            widget.additions_panel_geometry_animation = QPropertyAnimation(widget.additions_panel, b'geometry')
            widget.additions_panel_geometry_animation.setEasingCurve(QEasingCurve.OutCirc)

            widget.stacked_widget = QStackedWidget()
            widget.additions_panel.layout().addWidget(widget.stacked_widget, 1)

            widget.vertical_menu = QWidget()
            widget.vertical_menu.setLayout(QVBoxLayout())
            widget.vertical_menu.layout().setContentsMargins(0, 0, 0, 0)
            widget.vertical_menu.layout().setSpacing(0)
            widget.vertical_menu.setFixedWidth(45)
            widget.additions_panel.layout().addWidget(widget.vertical_menu)

        def resizeEvent(widget, event):
            widget.additions_panel.setGeometry(-(widget.width() - widget.additions_panel.property('size_x_when_hidden')) if widget.property('is_hidden') and not self.left_panel_addbutton.property('is_shown') else 0, 0, widget.width() - 10, widget.height())
            self.left_panel_addbutton.setGeometry(0 if self.left_panel_addbutton.property('is_shown') else -(widget.width() + self.left_panel_addbutton.property('x_offset')), widget.additions_panel.y() + 20, widget.width() + self.left_panel_addbutton.property('x_offset'), 50)
            return event

        def show_additions_panel(widget):
            widget.setProperty('is_hidden', False)
            generate_effect(widget.additions_panel_geometry_animation, 'geometry', 500, [widget.additions_panel.x(), 0, widget.additions_panel.width(), widget.additions_panel.height()], [0, 0, widget.additions_panel.width(), widget.additions_panel.height()])

        def hide_additions_panel(widget):
            widget.setProperty('is_hidden', True)
            generate_effect(widget.additions_panel_geometry_animation, 'geometry', 500, [widget.additions_panel.x(), widget.additions_panel.y(), widget.additions_panel.width(), widget.additions_panel.height()], [-(widget.width() - widget.additions_panel.property('size_x_when_hidden')), 0, widget.width() - 10, widget.height()])
            self.left_panel_addbutton.hide()

        def addition_clicked(widget, addition=False):
            for icon_widget in widget.vertical_menu.children():
                if isinstance(icon_widget, QPushButton) and not icon_widget.property('type') == addition.get('type', 'media'):
                    icon_widget.setChecked(False)
            if not addition or addition.get('type', 'media') == widget.selected_addition.get('type', 'media'):
                widget.hide_additions_panel()
                widget.selected_addition = {}
            elif addition:
                addition['show_widgets_function']()
                widget.show_additions_panel()
                widget.selected_addition = addition

    self.left_panel_widget = left_panel_widget()
    self.main_widget.layout().addWidget(self.left_panel_widget, 0, 0, 0, 2)

    class left_panel_addbutton(QWidget):
        def __init__(widget, parent=None):
            super().__init__(parent)
            widget.setProperty('x_offset', 12)
            widget.setProperty('is_shown', False)
            widget.setLayout(QHBoxLayout())
            widget.layout().setContentsMargins(0, 0, 0, 0)
            widget.layout().setSpacing(0)
            widget.geometry_animation = QPropertyAnimation(widget, b'geometry')
            widget.geometry_animation.setEasingCurve(QEasingCurve.OutCirc)

            widget.left_label = QLabel()
            widget.left_label.setObjectName('left_panel_addbutton_left_label')
            widget.left_label.setAlignment(Qt.AlignRight | Qt.AlignVCenter)
            widget.layout().addWidget(widget.left_label, 1)

            widget.add_button = QPushButton()
            widget.add_button.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Minimum)
            widget.add_button.setObjectName('left_panel_addbutton_add_button')
            widget.layout().addWidget(widget.add_button)

        def hide(widget):
            widget.setProperty('is_shown', False)
            generate_effect(widget.geometry_animation, 'geometry', 500, [widget.x(), widget.y(), widget.width(), widget.height()], [-(widget.width()), widget.y(), widget.width(), widget.height()])

        def show(widget):
            widget.setProperty('is_shown', True)
            widget.left_label.setText(_('left_panel.addition_types.' + self.left_panel_widget.selected_addition.get('type', 'media')))
            generate_effect(widget.geometry_animation, 'geometry', 500, [widget.x(), widget.y(), widget.width(), widget.height()], [0, widget.y(), widget.width(), widget.height()])

    self.left_panel_addbutton = left_panel_addbutton(self)
    self.left_panel_addbutton.add_button.clicked.connect(lambda: left_panel_addbutton_clicked(self))


def left_panel_addbutton_clicked(self):
    self.left_panel_widget.selected_addition['add_to_playlist_function']()


def translate(self):
    self.left_panel_addbutton.add_button.setText(_('left_panel.add_to_playlist'))

    # self.left_panel_addbutton.clicked.connect(lambda: left_panel_addbutton_clicked(self))

    # self.left_panel_widget.show_additions_panel()

# def load(self):
#     self.selected_addition = False

#     self.left_panel_media_to_add = []

#     self.left_panel_widget = QLabel(parent=self)
#     self.left_panel_widget.setAttribute(Qt.WA_OpaquePaintEvent)

#     # self.left_panel_widget.viewport().setAttribute(Qt.WA_OpaquePaintEvent)
#     # .setAttribute(Qt.WA_OpaquePaintEvent)
#     # self.left_panel_widget.setAttribute(Qt.WA_NoSystemBackground)

#     self.left_panel_widget.setObjectName('left_panel')
#     self.left_panel_widget.setStyleSheet('#left_panel { border-top: 75px; border-right: 60px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "left_panel_background.png").replace('\\', '/') + '") 75 60 0 0 stretch stretch; }')
#     self.left_panel_geometry_animation = QPropertyAnimation(self.left_panel_widget, b'geometry')
#     self.left_panel_geometry_animation.setEasingCurve(QEasingCurve.OutCirc)

#     self.left_panel_add_title = QLabel(parent=self.left_panel_widget)
#     self.left_panel_add_title.setObjectName('left_panel_add_title')
#     self.left_panel_add_title.setStyleSheet('#left_panel_add_title { padding:5px; font-family: "Ubuntu"; font-size:12pt; font-weight:bold; color:white; qproperty-alignment: "AlignRight | AlignVCenter"; border-top: 2px; border-bottom: 2px;  border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "add_title_background.png").replace('\\', '/') + '") 2 0 2 0 stretch stretch; }')

#     self.left_panel_icons = QWidget(parent=self.left_panel_widget)

#     left_panel_video.load(self)

#     image.load_left_panel(self)


# #     #######################################################################
# #     ## ADDITION PANEL - AUDIO

# #     class left_panel_audio_icon(QLabel):
# #         # def mouseReleaseEvent(widget, event):
# #         #    widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(PATH_PRESENTD_GRAPHICS, 'music_icon_hover.png').replace('\\', '/') + '); } ')
# #         def mousePressEvent(widget, event):
# #             if not self.selected_addition == 'audio':
# #                 self.selected_addition = 'audio'
# #                 show_additions(self)
# #             else:
# #                 self.selected_addition = False
# #                 hide_additions(self)
# #             widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(PATH_PRESENTD_GRAPHICS, 'music_icon_pressed.png').replace('\\', '/') + '); } ')

# #         def enterEvent(widget, event):
# #             if not self.selected_addition == 'audio':
# #                 widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(PATH_PRESENTD_GRAPHICS, 'music_icon_hover.png').replace('\\', '/') + '); } ')

# #         def leaveEvent(widget, event):
# #             if not self.selected_addition == 'audio':
# #                 widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(PATH_PRESENTD_GRAPHICS, 'music_icon_normal.png').replace('\\', '/') + '); } ')

# #     self.left_panel_audio_icon = left_panel_audio_icon(parent=self.left_panel_icons)
# #     self.left_panel_audio_icon.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(PATH_PRESENTD_GRAPHICS, 'music_icon_normal.png').replace('\\', '/') + '); } ')

# #     self.left_panel_audio_panel = QWidget(self.left_panel_widget)

# #     self.left_panel_audio_alert = QLabel(u'Parece que não há arquivos de cânticos importados no kihvim. Adicione os cânticos usando o botão abaixo.',parent=self.left_panel_audio_panel)
# #     self.left_panel_audio_alert.setWordWrap(True)
# #     self.left_panel_audio_alert.setStyleSheet('QLabel {padding: 50px; font-family: "Ubuntu"; font-size:12pt; color:gray; qproperty-alignment:"AlignCenter" ; }')

# #     self.left_panel_audio_list = QListWidget(parent=self.left_panel_audio_panel)
# #     self.left_panel_audio_list.setViewMode(QListView.IconMode)
# #     self.left_panel_audio_list.setIconSize(QSize(48, 48))
# #     self.left_panel_audio_list.setSpacing(5)
# #     self.left_panel_audio_list.currentItemChanged.connect(lambda:left_panel_audio_list_clicked(self))

# #     self.left_panel_audio_download_lyrics = QPushButton(parent=self.left_panel_audio_panel)
# #     self.left_panel_audio_download_lyrics.setIconSize(QSize(18, 18))
# #     self.left_panel_audio_download_lyrics.setIcon(QIcon(os.path.join(PATH_PRESENTD_GRAPHICS, "get_web_icon.png")))
# #     self.left_panel_audio_download_lyrics.setStyleSheet('QPushButton { color:white; font-size:12px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
# #     self.left_panel_audio_download_lyrics.clicked.connect(lambda:left_panel_audio_download_lyrics_clicked(self))

# #     self.left_panel_audio_add = QPushButton(parent=self.left_panel_audio_panel)
# #     self.left_panel_audio_add.setIconSize(QSize(18, 18))
# #     self.left_panel_audio_add.setIcon(QIcon(os.path.join(PATH_PRESENTD_GRAPHICS, "add_icon.png")))
# #     self.left_panel_audio_add.setStyleSheet('QPushButton { color:white; font-size:12px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
# #     self.left_panel_audio_add.clicked.connect(lambda:left_panel_audio_add_clicked(self))

# #     self.left_panel_audio_edit = QPushButton(parent=self.left_panel_audio_panel)
# #     self.left_panel_audio_edit.setCheckable(True)
# #     self.left_panel_audio_edit.setIconSize(QSize(18, 18))
# #     self.left_panel_audio_edit.setIcon(QIcon(os.path.join(PATH_PRESENTD_GRAPHICS, "edit_icon.png")))
# #     self.left_panel_audio_edit.setStyleSheet('QPushButton { color:white; font-size:12px; border-top: 5px; border-right: 0px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 0px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 0px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:checked { border-top: 5px; border-right: 0px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
# #     self.left_panel_audio_edit.clicked.connect(lambda:left_panel_audio_edit_clicked(self))

# #     self.left_panel_audio_remove = QPushButton(parent=self.left_panel_audio_panel)
# #     self.left_panel_audio_remove.setIconSize(QSize(18, 18))
# #     self.left_panel_audio_remove.setIcon(QIcon(os.path.join(PATH_PRESENTD_GRAPHICS, "remove_icon.png")))
# #     self.left_panel_audio_remove.setStyleSheet('QPushButton { color:white; font-size:12px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 0px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 0px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 0px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
# #     self.left_panel_audio_remove.clicked.connect(lambda:left_panel_audio_remove_clicked(self))

# #     self.left_panel_audio_edit_panel = QWidget(parent=self.left_panel_audio_panel)

# #     self.left_panel_audio_edit_number_label = QLabel(u'NÚMERO',parent=self.left_panel_audio_edit_panel)
# #     self.left_panel_audio_edit_number_label.setStyleSheet('QLabel {font-family: "Ubuntu"; font-size:10px; color:black; qproperty-alignment:"AlignLeft" ; }')

# #     self.left_panel_audio_edit_number = QLabel(parent=self.left_panel_audio_edit_panel)
# #     self.left_panel_audio_edit_number.setStyleSheet('QLabel {font-family: "Ubuntu"; font-weight:bold;  font-size:20px; color:black; qproperty-alignment:"AlignLeft" ; }')

# #     self.left_panel_audio_edit_title_label = QLabel(u'TÍTULO',parent=self.left_panel_audio_edit_panel)
# #     self.left_panel_audio_edit_title_label.setStyleSheet('QLabel {font-family: "Ubuntu"; font-size:10px; color:black; qproperty-alignment:"AlignLeft" ; }')

# #     self.left_panel_audio_edit_title = QLineEdit(parent=self.left_panel_audio_edit_panel)

# #     self.left_panel_audio_edit_filepath_label = QLabel(u'CAMINHO DO ARQUIVO',parent=self.left_panel_audio_edit_panel)
# #     self.left_panel_audio_edit_filepath_label.setStyleSheet('QLabel {font-family: "Ubuntu"; font-size:10px; color:black; qproperty-alignment:"AlignLeft" ; }')

# #     self.left_panel_audio_edit_filepath = QLabel(parent=self.left_panel_audio_edit_panel)
# #     self.left_panel_audio_edit_filepath.setStyleSheet('QLabel {font-family: "Ubuntu"; font-size:20px; color:black; qproperty-alignment:"AlignLeft" ; }')

# #     self.left_panel_audio_edit_lyrics_label = QLabel(u'LETRA',parent=self.left_panel_audio_edit_panel)
# #     self.left_panel_audio_edit_lyrics_label.setStyleSheet('QLabel {font-family: "Ubuntu"; font-size:10px; color:black; qproperty-alignment:"AlignLeft" ; }')

# #     class left_panel_audio_edit_lyrics(QPlainTextEdit):
# #         def enterEvent(widget, event):
# #             self.left_panel_audio_edit_lyrics_get_from_web.setVisible(True)
# #         def leaveEvent(widget, event):
# #             self.left_panel_audio_edit_lyrics_get_from_web.setVisible(False)

# #     self.left_panel_audio_edit_lyrics = left_panel_audio_edit_lyrics(parent=self.left_panel_audio_edit_panel)
# #     self.left_panel_audio_edit_lyrics.setStyleSheet('QLabel {font-family: "Ubuntu"; font-size:12px; color:black; qproperty-alignment:"AlignLeft" ; }')

# #     self.left_panel_audio_edit_lyrics_get_from_web = QPushButton(parent=self.left_panel_audio_edit_lyrics)
# #     self.left_panel_audio_edit_lyrics_get_from_web.setIconSize(QSize(18, 18))
# #     self.left_panel_audio_edit_lyrics_get_from_web.setIcon(QIcon(os.path.join(PATH_PRESENTD_GRAPHICS, "get_web_icon.png")))
# #     self.left_panel_audio_edit_lyrics_get_from_web.setStyleSheet('QPushButton { color:white; font-size:12px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
# #     self.left_panel_audio_edit_lyrics_get_from_web.clicked.connect(lambda:left_panel_audio_edit_lyrics_get_from_web_clicked(self))
# #     self.left_panel_audio_edit_lyrics_get_from_web.setVisible(False)

# #     #######################################################################
# #     ## ADDITION PANEL - IMAGE

# #     class left_panel_image_icon(QLabel):
# #         #def mouseReleaseEvent(widget, event):
# #         #    widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(PATH_PRESENTD_GRAPHICS, 'image_icon_hover.png').replace('\\', '/') + '); } ')
# #         def mousePressEvent(widget, event):
# #             if not self.selected_addition == 'image':
# #                 self.selected_addition = 'image'
# #                 show_additions(self)
# #             else:
# #                 self.selected_addition = False
# #                 hide_additions(self)
# #             widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(PATH_PRESENTD_GRAPHICS, 'image_icon_pressed.png').replace('\\', '/') + '); } ')
# #         def enterEvent(widget, event):
# #             if not self.selected_addition == 'image':
# #                 widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(PATH_PRESENTD_GRAPHICS, 'image_icon_hover.png').replace('\\', '/') + '); } ')
# #         def leaveEvent(widget, event):
# #             if not self.selected_addition == 'image':
# #                 widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(PATH_PRESENTD_GRAPHICS, 'image_icon_normal.png').replace('\\', '/') + '); } ')

# #     self.left_panel_image_icon = left_panel_image_icon(parent=self.left_panel_icons)
# #     self.left_panel_image_icon.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(PATH_PRESENTD_GRAPHICS, 'image_icon_normal.png').replace('\\', '/') + '); } ')

# #     self.left_panel_image_panel = QWidget(self.left_panel_widget)

# #     self.left_panel_image_tab = QTabWidget(parent=self.left_panel_image_panel)
# #     self.left_panel_image_tab.setTabPosition(QTabWidget.North)
# #     self.left_panel_image_tab.currentChanged.connect(lambda:left_panel_image_tab_changed(self))

# #     self.left_panel_image_tab_local = QWidget(parent=self)

# #     self.left_panel_image_alert = QLabel(u'Parece que não há arquivos de imagens disponíveis na pasta selecionada. Adicione imagens usando o botão abaixo.',parent=self.left_panel_image_tab_local)
# #     self.left_panel_image_alert.setWordWrap(True)
# #     self.left_panel_image_alert.setStyleSheet('QLabel {padding: 50px; font-family: "Ubuntu"; font-size:12pt; color:gray; qproperty-alignment:"AlignCenter" ; }')

# #     self.left_panel_image_list = QListWidget(parent=self.left_panel_image_tab_local)
# #     self.left_panel_image_list.setIconSize(QSize(56*self.screen_height_proportion, 56))
# #     self.left_panel_image_list.setSpacing(5)
# #     self.left_panel_image_list.currentItemChanged.connect(lambda:left_panel_image_list_clicked(self))

# #     self.left_panel_image_add = QPushButton(parent=self.left_panel_image_tab_local)
# #     self.left_panel_image_add.setIconSize(QSize(18, 18))
# #     self.left_panel_image_add.setIcon(QIcon(os.path.join(PATH_PRESENTD_GRAPHICS, "add_icon.png")))
# #     self.left_panel_image_add.setStyleSheet('QPushButton { color:white; font-size:12px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
# #     self.left_panel_image_add.clicked.connect(lambda:left_panel_image_add_clicked(self))

# #     class left_panel_image_path_label(QLabel):
# #         def enterEvent(widget, event):
# #             self.left_panel_image_edit.setVisible(True)
# #         def leaveEvent(widget, event):
# #             self.left_panel_image_edit.setVisible(False)

# #     self.left_panel_image_path_label = left_panel_image_path_label(parent=self.left_panel_image_tab_local)
# #     self.left_panel_image_path_label.setAlignment(Qt.AlignVCenter | Qt.AlignRight)

# #     self.left_panel_image_edit = QPushButton(parent=self.left_panel_image_path_label)
# #     self.left_panel_image_edit.setIconSize(QSize(18, 18))
# #     self.left_panel_image_edit.setIcon(QIcon(os.path.join(PATH_PRESENTD_GRAPHICS, "edit_icon.png")))
# #     self.left_panel_image_edit.setStyleSheet('QPushButton { color:white; font-size:12px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:checked { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
# #     self.left_panel_image_edit.clicked.connect(lambda:left_panel_image_edit_clicked(self))

# #     self.left_panel_image_remove = QPushButton(parent=self.left_panel_image_tab_local)
# #     self.left_panel_image_remove.setIconSize(QSize(18, 18))
# #     self.left_panel_image_remove.setIcon(QIcon(os.path.join(PATH_PRESENTD_GRAPHICS, "remove_icon.png")))
# #     self.left_panel_image_remove.setStyleSheet('QPushButton { color:white; font-size:12px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
# #     self.left_panel_image_remove.clicked.connect(lambda:left_panel_image_remove_clicked(self))

# #     self.left_panel_image_tab.blockSignals(True)
# #     self.left_panel_image_tab.addTab(self.left_panel_image_tab_local, 'DO COMPUTADOR')
# #     self.left_panel_image_tab.blockSignals(False)

# #     self.left_panel_image_tab_web = QWidget(parent=self)

# #     self.left_panel_image_keyword = QLineEdit(parent=self.left_panel_image_tab_web)
# #     self.left_panel_image_keyword.setText('')
# #     self.left_panel_image_keyword.setStyleSheet('QLabel {font-family: "Ubuntu"; font-weight:bold;  font-size:20px; color:black; qproperty-alignment:"AlignLeft" ; }')
# #     self.left_panel_image_keyword.textEdited.connect(lambda:left_panel_image_keyword_changed(self))

# #     self.left_panel_image_keyword_go = QPushButton(u'PESQUISAR', parent=self.left_panel_image_tab_web)
# #     self.left_panel_image_keyword_go.setStyleSheet('QPushButton { color:white; font-size:12px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 0px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 0px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 0px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
# #     self.left_panel_image_keyword_go.clicked.connect(lambda:left_panel_image_keyword_go_clicked(self))

# #     #self.left_panel_image_webview = QWebEngineView(parent=self.left_panel_image_tab_web)
# #     self.left_panel_image_webview = QWidget(parent=self.left_panel_image_tab_web)

# #     #class LeftWebView(QWebEngineView):
# #     #    def createWindow(widget, windowType):

# #             #if windowType == QWebEnginePage.WebBrowserTab:
# #             #    self = LeftWebView()
# #             #    print('2')
# #             #    self.setAttribute(Qt.WA_DeleteOnClose, True)
# #             #    print('3')
# #             #    self.show()
# #             #    print('4')
# #             #    #return self.LeftWebView
# #     #        return widget
# #             #    print('5')
# #             #return super(LeftWebView, self).createWindow(windowType)
# #             #print('6')
# #             #return widget#self.left_panel_image_keyword_load_finished(widget.url())

# #     #self.left_panel_image_webview = LeftWebView(parent=self.left_panel_image_tab_web)




# #     #self.left_panel_image_webview.settings().setAttribute(QWebEngineSettings.JavascriptEnabled, True)
# #     #self.left_panel_image_webview.settings().setAttribute(QWebEngineSettings.JavascriptCanOpenWindows, True)
# #     #self.left_panel_image_webview.page().setLinkDelegationPolicy(QWebEnginePage.DelegateAllLinks)
# #     #ui->webView->settings()->setAttribute(QWebSettings::JavascriptCanOpenWindows, true);
# #     #self.left_panel_image_webview.loadFinished.connect(lambda:left_panel_image_keyword_load_finished(self))
# #     #self.left_panel_image_webview.linkClicked.connect(lambda:left_panel_image_keyword_load_finished(self))

# #     self.left_panel_image_webview_warning = QLabel(u'Faça uma pesquisa de imagens usando o campo acima. Ao encontrar a imagem desejada, clique no link que abrirá a imagem (geralmente algo como "Visualizar arquivo").',parent=self.left_panel_image_tab_web)
# #     self.left_panel_image_webview_warning.setWordWrap(True)
# #     self.left_panel_image_webview_warning.setStyleSheet('QLabel {padding: 50px; font-family: "Ubuntu"; font-size:12pt; color:gray; qproperty-alignment:"AlignCenter" ; }')

# #     class left_panel_image_webview_preview(QLabel):
# #         def enterEvent(widget, event):
# #             self.left_panel_image_webview_preview_cancel_button.setVisible(True)
# #         def leaveEvent(widget, event):
# #             self.left_panel_image_webview_preview_cancel_button.setVisible(False)

# #     self.left_panel_image_webview_preview = left_panel_image_webview_preview(parent=self.left_panel_image_tab_web)
# #     self.left_panel_image_webview_preview.setWordWrap(True)
# #     self.left_panel_image_webview_preview.setObjectName('left_panel_image_webview_preview')
# #     self.left_panel_image_webview_preview.setStyleSheet('#left_panel_image_webview_preview { border-top: 4px; border-right: 4px; border-bottom: 4px; border-left: 4px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "black_box.png").replace('\\', '/') + '") 4 4 4 4 stretch stretch; }')

# #     self.left_panel_image_webview_preview_cancel_button = QPushButton(u'CANCELAR', parent=self.left_panel_image_webview_preview)
# #     self.left_panel_image_webview_preview_cancel_button.setStyleSheet('QPushButton { color:white; font-size:12px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:checked { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
# #     self.left_panel_image_webview_preview_cancel_button.clicked.connect(lambda:left_panel_image_webview_preview_cancel_button_clicked(self))

# #     self.left_panel_image_tab.blockSignals(True)
# #     #self.left_panel_image_tab.addTab(self.left_panel_image_tab_web, 'DO JW.ORG')
# #     self.left_panel_image_tab.blockSignals(False)



# #     #######################################################################
# #     ## ADDITION PANEL - WEB

# #     class left_panel_web_icon(QLabel):
# #         #def mouseReleaseEvent(widget, event):
# #         #    widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(PATH_PRESENTD_GRAPHICS, 'web_icon_hover.png').replace('\\', '/') + '); } ')
# #         def mousePressEvent(widget, event):
# #             if not self.selected_addition == 'web':
# #                 self.selected_addition = 'web'
# #                 show_additions(self)
# #             else:
# #                 self.selected_addition = False
# #                 hide_additions(self)
# #             widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(PATH_PRESENTD_GRAPHICS, 'web_icon_pressed.png').replace('\\', '/') + '); } ')
# #         def enterEvent(widget, event):
# #             if not self.selected_addition == 'web':
# #                 widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(PATH_PRESENTD_GRAPHICS, 'web_icon_hover.png').replace('\\', '/') + '); } ')
# #         def leaveEvent(widget, event):
# #             if not self.selected_addition == 'web':
# #                 widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(PATH_PRESENTD_GRAPHICS, 'web_icon_normal.png').replace('\\', '/') + '); } ')

# #     self.left_panel_web_icon = left_panel_web_icon(parent=self.left_panel_icons)
# #     self.left_panel_web_icon.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(PATH_PRESENTD_GRAPHICS, 'web_icon_normal.png').replace('\\', '/') + '); } ')

# #     self.left_panel_web_panel = QWidget(self.left_panel_widget)

# #     self.left_panel_web_address_label = QLabel(u'ENDEREÇO',parent=self.left_panel_web_panel)
# #     self.left_panel_web_address_label.setStyleSheet('QLabel {font-family: "Ubuntu"; font-size:10px; color:black; qproperty-alignment:"AlignLeft" ; }')

# #     self.left_panel_web_address = QLineEdit(parent=self.left_panel_web_panel)
# #     self.left_panel_web_address.setStyleSheet('QLabel {font-family: "Ubuntu"; font-weight:bold;  font-size:20px; color:black; qproperty-alignment:"AlignLeft" ; }')

# #     self.left_panel_web_address_go = QPushButton(u'TESTAR', parent=self.left_panel_web_panel)
# #     self.left_panel_web_address_go.setStyleSheet('QPushButton { color:white; font-size:12px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 0px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 0px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 0px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
# #     self.left_panel_web_address_go.clicked.connect(lambda:left_panel_web_address_go_clicked(self))

# #     #self.left_panel_web_webview = QWebEngineView(parent=self.left_panel_web_panel)

# #     #######################################################################
# #     ## ADDITION PANEL - CLOCK

# #     class left_panel_clock_icon(QLabel):
# #         #def mouseReleaseEvent(widget, event):
# #         #    widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(PATH_PRESENTD_GRAPHICS, 'clock_icon_hover.png').replace('\\', '/') + '); } ')
# #         def mousePressEvent(widget, event):
# #             if not self.selected_addition == 'clock':
# #                 self.selected_addition = 'clock'
# #                 show_additions(self)
# #             else:
# #                 self.selected_addition = False
# #                 hide_additions(self)
# #             widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(PATH_PRESENTD_GRAPHICS, 'clock_icon_pressed.png').replace('\\', '/') + '); } ')
# #         def enterEvent(widget, event):
# #             if not self.selected_addition == 'clock':
# #                 widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(PATH_PRESENTD_GRAPHICS, 'clock_icon_hover.png').replace('\\', '/') + '); } ')
# #         def leaveEvent(widget, event):
# #             if not self.selected_addition == 'clock':
# #                 widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(PATH_PRESENTD_GRAPHICS, 'clock_icon_normal.png').replace('\\', '/') + '); } ')

# #     self.left_panel_clock_icon = left_panel_clock_icon(parent=self.left_panel_icons)
# #     self.left_panel_clock_icon.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(PATH_PRESENTD_GRAPHICS, 'clock_icon_normal.png').replace('\\', '/') + '); } ')

# #     self.left_panel_clock_panel = QWidget(self.left_panel_widget)

# #     self.left_panel_clock_show_clock = QCheckBox(u'Mostrar relógio', parent=self.left_panel_clock_panel)
# #     #self.left_panel_clock_show_clock.clicked.connect(lambda:self.main_options_settings_enable_animations_clicked())

# #     self.left_panel_clock_show_song = QCheckBox(u'Mostrar título do cântico', parent=self.left_panel_clock_panel)
# #     #self.left_panel_clock_show_song.clicked.connect(lambda:self.main_options_settings_enable_animations_clicked())

# #     self.left_panel_clock_start_silent = QCheckBox(u'Iniciar mudo', parent=self.left_panel_clock_panel)
# #     #self.left_panel_clock_show_song.clicked.connect(lambda:self.main_options_settings_enable_animations_clicked())


# #     #######################################################################
# #     ## ADDITION PANEL - SCREENCOPY

# #     class left_panel_screencopy_icon(QLabel):
# #         def mousePressEvent(widget, event):
# #             if not self.selected_addition == 'screencopy':
# #                 self.selected_addition = 'screencopy'
# #                 show_additions(self)
# #             else:
# #                 self.selected_addition = False
# #                 hide_additions(self)
# #             widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(PATH_PRESENTD_GRAPHICS, 'screencopy_icon_pressed.png').replace('\\', '/') + '); } ')
# #         def enterEvent(widget, event):
# #             if not self.selected_addition == 'screencopy':
# #                 widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(PATH_PRESENTD_GRAPHICS, 'screencopy_icon_hover.png').replace('\\', '/') + '); } ')
# #         def leaveEvent(widget, event):
# #             if not self.selected_addition == 'screencopy':
# #                 widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(PATH_PRESENTD_GRAPHICS, 'screencopy_icon_normal.png').replace('\\', '/') + '); } ')

# #     self.left_panel_screencopy_icon = left_panel_screencopy_icon(parent=self.left_panel_icons)
# #     self.left_panel_screencopy_icon.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(PATH_PRESENTD_GRAPHICS, 'screencopy_icon_normal.png').replace('\\', '/') + '); } ')

# #     self.left_panel_screencopy_panel = QWidget(self.left_panel_widget)

# #     self.left_panel_screencopy_preview_background = QLabel(self.left_panel_screencopy_panel)
# #     self.left_panel_screencopy_preview_background.setObjectName('left_panel_screencopy_preview_background')
# #     self.left_panel_screencopy_preview_background.setStyleSheet('#left_panel_screencopy_preview_background { border-top: 4px; border-right: 4px; border-bottom: 4px; border-left: 4px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "black_box.png").replace('\\', '/') + '") 4 4 4 4 stretch stretch; }')


# #     self.left_panel_screencopy_preview = QLabel(self.left_panel_screencopy_preview_background)
# #     self.left_panel_screencopy_preview.setStyleSheet('QLabel {font-family: "Ubuntu"; font-size:12pt; color:gray; qproperty-alignment:"AlignCenter" ; }')

# #     self.left_panel_screencopy_description = QLabel(self.left_panel_screencopy_panel)

# #     self.left_panel_screencopy_take_button = QPushButton(u'DEFINIR CÓPIA', self.left_panel_screencopy_panel)
# #     self.left_panel_screencopy_take_button.setStyleSheet('QPushButton { color:white; font-size:12px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
# #     self.left_panel_screencopy_take_button.clicked.connect(lambda:left_panel_screencopy_take_button_clicked(self))

# #     #######################################################################
# #     ## ADDITION PANEL - CAMERA

# #     class left_panel_camera_icon(QLabel):
# #         def mousePressEvent(widget, event):
# #             if not self.selected_addition == 'camera':
# #                 self.selected_addition = 'camera'
# #                 show_additions(self)
# #             else:
# #                 self.selected_addition = False
# #                 hide_additions(self)
# #             widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(PATH_PRESENTD_GRAPHICS, 'camera_icon_pressed.png').replace('\\', '/') + '); } ')
# #         def enterEvent(widget, event):
# #             if not self.selected_addition == 'camera':
# #                 widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(PATH_PRESENTD_GRAPHICS, 'camera_icon_hover.png').replace('\\', '/') + '); } ')
# #         def leaveEvent(widget, event):
# #             if not self.selected_addition == 'camera':
# #                 widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(PATH_PRESENTD_GRAPHICS, 'camera_icon_normal.png').replace('\\', '/') + '); } ')

# #     self.left_panel_camera_icon = left_panel_camera_icon(parent=self.left_panel_icons)
# #     self.left_panel_camera_icon.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(PATH_PRESENTD_GRAPHICS, 'camera_icon_normal.png').replace('\\', '/') + '); } ')

# #     self.left_panel_camera_panel = QWidget(self.left_panel_widget)

# #     self.left_panel_camera_select = QComboBox(self.left_panel_camera_panel)
# #     self.left_panel_camera_select.activated.connect(lambda:left_panel_camera_select_selected(self))

# #     self.left_panel_camera_test_button = QPushButton(u'TESTAR', self.left_panel_camera_panel)
# #     self.left_panel_camera_test_button.setStyleSheet('QPushButton { color:white; font-size:12px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
# #     self.left_panel_camera_test_button.clicked.connect(lambda:left_panel_camera_test_button_clicked(self))

# #     self.left_panel_camera_preview = QLabel(self.left_panel_camera_panel)
# #     self.left_panel_camera_preview.setStyleSheet('QLabel {font-family: "Ubuntu"; font-size:12pt; color:gray; qproperty-alignment:"AlignCenter" ; }')

# #     #self.camera_instance = vlc.Instance()
# #     #self.left_panel_camera_preview_mediaplayer = False
# #     #self.left_panel_camera_preview_mediaplayer = video_instance.media_player_new()

# #     #self.left_panel_camera_preview_mediaplayer_widget = QFrame(parent=self)
# #     #self.left_panel_camera_preview_mediaplayer_widget.setGeometry(0,0,self.width(),self.height())
# #     #self.left_panel_camera_preview_mediaplayer.set_xwindow(self.left_panel_camera_preview_mediaplayer_widget.winId())
# #     #self.left_panel_camera_preview_mediaplayer.set_xwindow(self.left_panel_camera_preview.winId())

# #     self.left_panel_logo = QLabel(parent=self)
# #     self.left_panel_logo.setObjectName('left_panel_logo')
# #     self.left_panel_logo.setStyleSheet('#left_panel_logo { border-bottom: 70px; border-right: 55px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "left_panel_logo.png").replace('\\', '/') + '") 0 55 70 0 stretch stretch; }')
# #     self.left_panel_logo.setAttribute(Qt.WA_TransparentForMouseEvents)
# #     #self.left_panel_logo.setVisible(False)

# #     #self.left_panel_logo_test_button_player = video_instance.media_player_new()
# #     #self.left_panel_logo_test_button_player.set_media(video_instance.media_new(os.path.join(path_kihvim, 'resources',  'test_song.flac')))

# #     class left_panel_logo_test_button(QLabel):
# #         def mousePressEvent(widget, event):
# #             self.left_panel_logo_test_button_player.stop()
# #             self.generate_effect(app.monitor.test_widget_opacity_animation, 'opacity', 500, 1.0, 0.0)
# #             self.left_panel_logo_test_button_player.play()


# #     self.left_panel_logo_test_button = left_panel_logo_test_button(parent=self)
# #     #self.left_panel_logo_test_button.setObjectName('left_panel_logo_test_button')
# #     self.left_panel_logo_test_button.setPixmap(QPixmap(os.path.join(PATH_PRESENTD_GRAPHICS, "kihvim_corner_test_logo.png")))
# #     #self.left_panel_logo_test_button.setStyleSheet('#left_panel_logo_test_button { image: url("' + .replace('\\', '/') + '") }')
# #     #self.left_panel_logo_test_button.setMask(self.left_panel_logo_test_button.pixmap().mask())

# def resize(self):
#
#     self.left_panel_add_title.setGeometry(0, 20, self.left_panel_widget.width() - 183, 50)
#     self.left_panel_addbutton_label.setGeometry(0, 0, self.left_panel_addbutton.width(), self.left_panel_addbutton.height())
#     self.left_panel_icons.setGeometry(self.left_panel_widget.width() - 55, 70, 45, self.left_panel_widget.height() - 70)

#     self.left_panel_icon_y_position = 0
#     left_panel_video.resize(self)
#     image.resize_left_panel(self)

#     self.left_panel_icon_y_position += 40

# #     self.left_panel_audio_icon.setGeometry(0,0,45,45)
# #     self.left_panel_audio_panel.setGeometry(12,70,self.left_panel_widget.width()-12-55,self.left_panel_widget.height()-70)
# #     self.left_panel_audio_list.setGeometry(10,10,self.left_panel_audio_panel.width()-20,self.left_panel_audio_panel.height()-80)
# #     self.left_panel_audio_alert.setGeometry(10,10,self.left_panel_audio_panel.width()-20,self.left_panel_audio_panel.height()-80)
# #     self.left_panel_audio_edit.setGeometry(10+self.left_panel_audio_list.width()-80,10+self.left_panel_audio_list.height()+5,40,40)
# #     self.left_panel_audio_remove.setGeometry(10+self.left_panel_audio_list.width()-40,10+self.left_panel_audio_list.height()+5,40,40)
# #     self.left_panel_audio_download_lyrics.setGeometry(10+self.left_panel_audio_list.width()-125,10+self.left_panel_audio_list.height()+5,40,40)

# #     if self.left_panel_audio_list.count() > 0:
# #         self.left_panel_audio_add.setGeometry(10+self.left_panel_audio_list.width()-170,10+self.left_panel_audio_list.height()+5,40,40)
# #         #self.left_panel_audio_remove.setVisible(True)
# #     else:
# #         self.left_panel_audio_add.setGeometry(self.left_panel_audio_panel.width()*.4,self.left_panel_audio_panel.height()*.6,self.left_panel_audio_panel.width()*.2,self.left_panel_audio_panel.height()*.1)
# #         #self.left_panel_audio_remove.setVisible(False)

# #     self.left_panel_audio_edit_panel.setGeometry(10,10,self.left_panel_audio_panel.width()-20,self.left_panel_audio_panel.height()-80)
# #     self.left_panel_audio_edit_number_label.setGeometry(0,0,self.left_panel_audio_edit_panel.width()*.2,20)
# #     self.left_panel_audio_edit_number.setGeometry(0,20,self.left_panel_audio_edit_panel.width()*.2,30)
# #     self.left_panel_audio_edit_title_label.setGeometry(self.left_panel_audio_edit_panel.width()*.2,0,self.left_panel_audio_edit_panel.width()*.8,20)
# #     self.left_panel_audio_edit_title.setGeometry(self.left_panel_audio_edit_panel.width()*.2,20,self.left_panel_audio_edit_panel.width()*.8,30)
# #     self.left_panel_audio_edit_filepath_label.setGeometry(0,60,self.left_panel_audio_edit_panel.width(),20)
# #     self.left_panel_audio_edit_filepath.setGeometry(0,80,self.left_panel_audio_edit_panel.width(),30)
# #     self.left_panel_audio_edit_lyrics_label.setGeometry(0,120,self.left_panel_audio_edit_panel.width(),20)
# #     self.left_panel_audio_edit_lyrics.setGeometry(0,140,self.left_panel_audio_edit_panel.width(),self.left_panel_audio_edit_panel.height()-160)
# #     self.left_panel_audio_edit_lyrics_get_from_web.setGeometry(self.left_panel_audio_edit_lyrics.width()-50,self.left_panel_audio_edit_lyrics.height()-50,40,40)

# #     self.left_panel_web_icon.setGeometry(0,135,45,45)
# #     self.left_panel_web_panel.setGeometry(12,70,self.left_panel_widget.width()-12-55,self.left_panel_widget.height()-70)
# #     self.left_panel_web_address_label.setGeometry(10,10,(self.left_panel_web_panel.width()-20)*.8,20)
# #     self.left_panel_web_address.setGeometry(10,30,(self.left_panel_web_panel.width()-20)*.8,30)
# #     self.left_panel_web_address_go.setGeometry(10+((self.left_panel_web_panel.width()-20)*.8),30,(self.left_panel_web_panel.width()-20)*.2,30)
# #     #self.left_panel_web_webview.setGeometry(10,70,self.left_panel_web_panel.width()-20, self.left_panel_web_panel.height()-80)

# #     self.left_panel_clock_icon.setGeometry(0,180,45,45)
# #     self.left_panel_clock_panel.setGeometry(12,70,self.left_panel_widget.width()-12-55,self.left_panel_widget.height()-70)
# #     self.left_panel_clock_show_clock.setGeometry(10,10,self.left_panel_web_panel.width()-20,20)
# #     self.left_panel_clock_show_song.setGeometry(10,30,self.left_panel_web_panel.width()-20,20)
# #     self.left_panel_clock_start_silent.setGeometry(10,50,self.left_panel_web_panel.width()-20,20)

# #     self.left_panel_screencopy_icon.setGeometry(0,225,45,45)
# #     self.left_panel_screencopy_panel.setGeometry(12,70,self.left_panel_widget.width()-12-55,self.left_panel_widget.height()-70)
# #     self.left_panel_screencopy_preview_background.setGeometry(10,10,self.left_panel_screencopy_panel.width()-20,(self.left_panel_screencopy_panel.width()-20)*self.screen_width_proportion)
# #     self.left_panel_screencopy_preview.setGeometry(3,3,self.left_panel_screencopy_preview_background.width()-6,(self.left_panel_screencopy_preview_background.width()-6)*self.screen_width_proportion)
# #     self.left_panel_screencopy_description.setGeometry(10,20+self.left_panel_screencopy_preview.height(),self.left_panel_screencopy_preview.width()*.75,30)
# #     self.left_panel_screencopy_take_button.setGeometry(10+(self.left_panel_screencopy_preview_background.width()*.75),20+self.left_panel_screencopy_preview_background.height(),self.left_panel_screencopy_preview_background.width()*.25,30)

# #     self.left_panel_camera_icon.setGeometry(0,270,45,45)
# #     self.left_panel_camera_panel.setGeometry(12,70,self.left_panel_widget.width()-12-55,self.left_panel_widget.height()-70)
# #     self.left_panel_camera_select.setGeometry(10,10,(self.left_panel_camera_panel.width()-20)*.75,30)
# #     self.left_panel_camera_test_button.setGeometry(10+((self.left_panel_camera_panel.width()-20)*.75),10,(self.left_panel_camera_panel.width()-20)*.25,30)
# #     self.left_panel_camera_preview.setGeometry(10,50,self.left_panel_camera_panel.width()-20,(self.left_panel_camera_panel.width()-20)*self.screen_width_proportion)



# def show_additions(self):
#     self.left_panel_addbutton.setEnabled(False)
#     self.left_panel_addbutton_label.setText(u'<font style="font-size:9px;">' + u'SELECIONE ALGO PARA' + '</font><br><font style="font-size:14px;"><b>' + u'ADICIONAR' + '</b></font>')

#     if self.settings.get('enable_animations', True):
#         self.generate_effect(self.left_panel_geometry_animation, 'geometry', 500, [self.left_panel_widget.x(),0,self.width()*.5,self.height()], [0,0,self.width()*.5,self.height()])
#         self.generate_effect(self.left_panel_addbutton_geometry_animation, 'geometry', 500, [self.left_panel_addbutton.x(),self.left_panel_addbutton.y(),self.left_panel_addbutton.width(),self.left_panel_addbutton.height()], [(self.width()*.5)-183,self.left_panel_addbutton.y(),self.left_panel_addbutton.width(),self.left_panel_addbutton.height()])
#     else:
#         self.left_panel_widget.setGeometry(0,0,self.width()*.5,self.height())
#         self.left_panel_addbutton.setGeometry((self.width()*.5)-183,self.left_panel_addbutton.y(),self.left_panel_addbutton.width(),self.left_panel_addbutton.height())

#     # if not self.selected_addition == 'audio':
#     #     self.left_panel_audio_icon.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(PATH_PRESENTD_GRAPHICS, 'music_icon_normal.png').replace('\\', '/') + '); } ')
#     #     self.left_panel_audio_panel.setVisible(False)
#     if not self.selected_addition == 'image':
#         self.left_panel_image_icon.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(PATH_PRESENTD_GRAPHICS, 'image_icon_normal.png').replace('\\', '/') + '); } ')
#         self.left_panel_image_panel.setVisible(False)
#     if not self.selected_addition == 'video':
#         left_panel_video.hide_adition(self)
#     # if not self.selected_addition == 'clock':
#     #     self.left_panel_clock_icon.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(PATH_PRESENTD_GRAPHICS, 'clock_icon_normal.png').replace('\\', '/') + '); } ')
#     #     self.left_panel_clock_panel.setVisible(False)
#     # if not self.selected_addition == 'web':
#     #     self.left_panel_web_icon.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(PATH_PRESENTD_GRAPHICS, 'web_icon_normal.png').replace('\\', '/') + '); } ')
#     #     self.left_panel_web_panel.setVisible(False)
#     # if not self.selected_addition == 'screencopy':
#     #     self.left_panel_screencopy_icon.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(PATH_PRESENTD_GRAPHICS, 'screencopy_icon_normal.png').replace('\\', '/') + '); } ')
#     #     self.left_panel_screencopy_panel.setVisible(False)
#     # if not self.selected_addition == 'camera':
#     #     self.left_panel_camera_icon.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(PATH_PRESENTD_GRAPHICS, 'camera_icon_normal.png').replace('\\', '/') + '); } ')
#     #     self.left_panel_camera_panel.setVisible(False)
#         #if not self.left_panel_camera_preview_mediaplayer.get_media() == None:
#         #    self.left_panel_camera_preview_mediaplayer.stop()
#         #    self.left_panel_camera_preview_mediaplayer.get_media().release()
#         #if self.left_panel_camera_preview_mediaplayer:
#         #    #print self.left_panel_camera_preview_mediaplayer
#         #    #self.left_panel_camera_preview_mediaplayer.get_media().release()
#         #    del self.left_panel_camera_preview_mediaplayer
#         #    self.left_panel_camera_preview_mediaplayer = False
#         #    #self.camera_instance.release()

#     # if self.selected_addition == 'audio':
#     #     self.left_panel_add_title.setText(u'CÂNTICO')
#     #     self.left_panel_audio_panel.setVisible(True)
#     # self.left_panel_audio_list.setCurrentItem(None)
#     if self.selected_addition == 'image':
#         self.left_panel_add_title.setText(u'IMAGEM')
#         self.left_panel_image_panel.setVisible(True)
#         self.left_panel_image_keyword.setText('')
#         # left_panel_image_tab_changed(self)
#     self.left_panel_image_list.setCurrentItem(None)
#     if self.selected_addition == 'video':
#         left_panel_video.show_addition(self)
#     # if self.selected_addition == 'screencopy':
#     #     self.left_panel_add_title.setText(u'CÓPIA DE TELA')
#     #     self.left_panel_screencopy_panel.setVisible(True)
#     #     self.left_panel_screencopy_preview.setPixmap(None)
#     #     self.left_panel_screencopy_preview.setText(u'Não há área para ser exibida.\nDefina uma área no botão abaixo.')
#     #     self.left_panel_screencopy_description.setText(u'<font style="font-size:9px;color:gray;">' + u'SEM ÁREA PARA MOSTRAR' + '</font>')
#     #     app.screencopy.selected_area = False
#     # if self.selected_addition == 'web':
#     #     self.left_panel_web_address.setText('http://www.jw.org')
#     #     self.left_panel_add_title.setText(u'PÁGINA DO JW.ORG')
#     #     self.left_panel_web_panel.setVisible(True)
#     # if self.selected_addition == 'clock':
#     #     self.left_panel_add_title.setText(u'PRELÚDIO')
#     #     self.left_panel_clock_panel.setVisible(True)
#     #     self.left_panel_addbutton.setEnabled(True)
#     #     self.left_panel_addbutton_label.setText(u'<font style="font-size:14px;"><b>' + u'ADICIONAR' + '</b></font><br><font style="font-size:9px;">' + u'NA LISTA DE EXECUÇÃO' + '</font>')
#     # if self.selected_addition == 'camera':
#     #     self.left_panel_add_title.setText(u'CAMERA')
#     #     self.left_panel_camera_panel.setVisible(True)
#     #     populate_camera_list(self)
#     #     self.left_panel_camera_select.setCurrentIndex(-1)
#     #     self.left_panel_camera_test_button.setEnabled(False)
#     #     #self.left_panel_camera_preview_mediaplayer = video_instance.media_player_new()
#     #     #self.left_panel_camera_preview_mediaplayer.set_xwindow(self.left_panel_camera_preview.winId())
#     #     self.left_panel_camera_preview.setText(u'Não há camera selecionada.\nDefina uma camera para testar  .')
#     # self.left_panel_video_local_list.setCurrentItem(None)


# def hide_additions(self):
#     if self.settings.get('enable_animations', True):
#         self.generate_effect(self.left_panel_geometry_animation, 'geometry', 500, [self.left_panel_widget.x(),0,self.width()*.5,self.height()], [67-(self.width()*.5),0,self.width()*.5,self.height()])
#         self.generate_effect(self.left_panel_addbutton_geometry_animation, 'geometry', 500, [self.left_panel_addbutton.x(),self.left_panel_addbutton.y(),self.left_panel_addbutton.width(),self.left_panel_addbutton.height()], [-196,self.left_panel_addbutton.y(),self.left_panel_addbutton.width(),self.left_panel_addbutton.height()])
#     else:
#         self.left_panel_widget.setGeometry(67-(self.width()*.5),0,self.width()*.5,self.height())
#         self.left_panel_addbutton.setGeometry(-196,self.left_panel_addbutton.y(),self.left_panel_addbutton.width(),self.left_panel_addbutton.height())


# def left_panel_addbutton_clicked(self):
#     playlist_panel.append_media_to_playlist(self, self.left_panel_media_to_add)


# def activate_add_button(self, media):
#     self.left_panel_addbutton.setEnabled(True)
#     self.left_panel_addbutton_label.setText(u'<font style="font-size:14px;"><b>' + 'Add'.upper() + '</b></font><br><font style="font-size:9px;">' + u'in the playlist'.upper() + '</font>')
#     self.left_panel_media_to_add = media