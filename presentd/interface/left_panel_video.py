#!/usr/bin/python3

import os
import subprocess

from PySide6.QtWidgets import QLabel, QWidget, QPushButton, QFileDialog, QListWidget, QListWidgetItem
from PySide6.QtCore import QSize
from PySide6.QtGui import QIcon, QPixmap

from presentd.paths import PATH_PRESENTD_GRAPHICS, REAL_PATH_HOME, FFMPEG_EXECUTABLE, PATH_TMP
from presentd.interface import left_panel

from mutagen.mp4 import MP4

def load(self):
    #######################################################################
    ## ADDITION PANEL - VIDEO

    class left_panel_video_icon(QLabel):
        #def mouseReleaseEvent(widget, event):
        #    widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(PATH_PRESENTD_GRAPHICS, 'video_icon_hover.png').replace('\\', '/') + '); } ')
        def mousePressEvent(widget, event):
            if not self.selected_addition == 'video':
                self.selected_addition = 'video'
                left_panel.show_additions(self)
            else:
                self.selected_addition = False
                left_panel.hide_additions(self)
            widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(PATH_PRESENTD_GRAPHICS, 'video_icon_pressed.png').replace('\\', '/') + '); } ')
        def enterEvent(widget, event):
            if not self.selected_addition == 'video':
                widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(PATH_PRESENTD_GRAPHICS, 'video_icon_hover.png').replace('\\', '/') + '); } ')
        def leaveEvent(widget, event):
            if not self.selected_addition == 'video':
                widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(PATH_PRESENTD_GRAPHICS, 'video_icon_normal.png').replace('\\', '/') + '); } ')

    self.left_panel_video_icon = left_panel_video_icon(parent=self.left_panel_icons)
    self.left_panel_video_icon.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(PATH_PRESENTD_GRAPHICS, 'video_icon_normal.png').replace('\\', '/') + '); } ')

    self.left_panel_video_panel = QWidget(self.left_panel_widget)

    self.left_panel_video_local_list = QListWidget(parent=self.left_panel_video_panel)
    self.left_panel_video_local_list.setIconSize(QSize(56*self.screen_height_proportion, 56))
    self.left_panel_video_local_list.setSpacing(5)
    self.left_panel_video_local_list.currentItemChanged.connect(lambda:left_panel_video_local_list_clicked(self))

    self.left_panel_video_add = QPushButton(parent=self.left_panel_video_panel)
    self.left_panel_video_add.setIconSize(QSize(18, 18))
    self.left_panel_video_add.setIcon(QIcon(os.path.join(PATH_PRESENTD_GRAPHICS, "add_icon.png")))
    self.left_panel_video_add.setStyleSheet('QPushButton { color:white; font-size:12px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
    self.left_panel_video_add.clicked.connect(lambda:left_panel_video_add_clicked(self))

    self.left_panel_video_remove = QPushButton(parent=self.left_panel_video_panel)
    self.left_panel_video_remove.setIconSize(QSize(18, 18))
    self.left_panel_video_remove.setIcon(QIcon(os.path.join(PATH_PRESENTD_GRAPHICS, "remove_icon.png")))
    self.left_panel_video_remove.setStyleSheet('QPushButton { color:white; font-size:12px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
    self.left_panel_video_remove.clicked.connect(lambda:left_panel_video_remove_clicked(self))

    update_left_panel_video_local_list(self)

def resize(self):
    self.left_panel_video_icon.setGeometry(0,self.left_panel_icon_y_position,45,45)
    self.left_panel_video_panel.setGeometry(12,70,self.left_panel_widget.width()-12-55,self.left_panel_widget.height()-70)
    self.left_panel_video_local_list.setGeometry(10,10,self.left_panel_video_panel.width()-20,self.left_panel_video_panel.height()-80)
    self.left_panel_video_remove.setGeometry(10+self.left_panel_video_local_list.width()-40,10+self.left_panel_video_local_list.height()+5,40,40)
    left_panel_video_add_set_geometry(self)


def hide_adition(self):
    self.left_panel_video_icon.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(PATH_PRESENTD_GRAPHICS, 'video_icon_normal.png').replace('\\', '/') + '); } ')
    self.left_panel_video_panel.setVisible(False)


def show_addition(self):
    self.left_panel_add_title.setText('Video'.upper())
    self.left_panel_video_panel.setVisible(True)
    self.left_panel_video_local_list.setCurrentItem(None)


def left_panel_video_add_set_geometry(self):
    if self.left_panel_video_local_list.count() > 0:
        self.left_panel_video_add.setGeometry(10+self.left_panel_video_local_list.width()-85,10+self.left_panel_video_local_list.height()+5,40,40)
    else:
        self.left_panel_video_add.setGeometry((self.left_panel_video_local_list.width()*.5)-100,(self.left_panel_video_local_list.height()*.5)-20,200,40)


def update_left_panel_video_local_list(self):
    final_filename_list = []
    for filename in self.settings['recent_media_files']['videos']:
        if os.path.isfile(filename):
            final_filename_list.append(filename)
        else:
            del filename

    if final_filename_list:
        self.left_panel_video_local_list.clear()

        for filename in sorted(final_filename_list):
            label = QLabel()

            video = MP4(filename)
            pixmap = QPixmap()
            if video.tags and 'covr' in video.tags:
                pixmap.loadFromData(video.tags['covr'][0])
            else:
                subprocess.call([FFMPEG_EXECUTABLE, '-loglevel', 'error', '-y', '-ss', '00:10.00', '-i', filename, '-frames:v', '1', os.path.join(PATH_TMP, 'thumbnail.png')])
                pixmap.load(os.path.join(PATH_TMP, 'thumbnail.png'))
            label.setPixmap(pixmap)

            label.setScaledContents(True)
            # label.setPixmap(sorted(list_filenames)[0][1])
            label.setGeometry(0,0,96,96)
            #label.setStyleSheet('QLabel {background-color:gray; font-family: "Ubuntu Condensed"; font-size:28px; color:white;  qproperty-alignment:"AlignCenter";}')
            item = QListWidgetItem(QIcon(label.pixmap()), filename)
            item.filepath = filename
            item.thumbnail = pixmap
            #item.setToolTip(filename)

            self.left_panel_video_local_list.addItem(item)

        self.left_panel_video_add.setText('')
        self.left_panel_video_local_list.setVisible(True)
        self.left_panel_video_remove.setVisible(True)
    else:
        self.left_panel_video_add.setText('Add videos')
        self.left_panel_video_local_list.setVisible(False)
        self.left_panel_video_remove.setVisible(False)

    left_panel_video_add_set_geometry(self)


def left_panel_video_add_clicked(self):
    image_path_list = QFileDialog.getOpenFileNames(self, 'Select the media files to add', REAL_PATH_HOME, "Video files (*.m4v *.mp4 *.M4V *.MP4)")[0]
    for filename in image_path_list:
        if os.path.isfile(filename):
            self.settings['recent_media_files']['videos'].append(filename)



            #print(video.tags["covr"][0])

            #icon = False
            # filename_hash = False
            #title = False
            #if filename.split('.')[-1] in ['m4v', 'mp4', 'M4V', 'MP4']:
                #     md5 = hashlib.md5()
                #     md5.update(open(filename, 'rb').read())
                #     filename_hash = md5.hexdigest()
                # if not os.path.isfile(os.path.join(path_tmp , filename_hash + '.png')):
                #     video = MP4(filename)
            #         print(video)

            #         if video.tags and 'covr' in [*video.tags]:
            #             artwork = video.tags["covr"][0]
            #             open(os.path.join(path_tmp, filename.split('/')[-1] + '.png'), 'wb').write(artwork)
            #         else:
            #             subprocess.call([ffmpeg_bin, '-loglevel', 'error', '-y', '-ss', '00:10.00', '-i', filename, '-frames:v', '1', os.path.join(path_tmp, filename.split('/')[-1] + '.png')])

            #         create_thumbnail(os.path.join(path_tmp, filename.split('/')[-1] + '.png'), os.path.join(path_tmp , filename_hash + '.png'),int(56*self.screen_height_proportion), 56)

            #         if '\xa9nam' in  [*video.tags]:
            #             title = video.tags['\xa9nam'][0]

            #     icon = os.path.join(path_tmp , filename_hash + '.png')

            # if icon and filename_hash:
            #     self.list_of_videos_added[filename_hash] = [icon, filename, title]

    update_left_panel_video_local_list(self)


def left_panel_video_remove_clicked(self):
    None


def left_panel_video_local_list_clicked(self):
    if self.left_panel_video_local_list.currentItem():
        duration = MP4(self.left_panel_video_local_list.currentItem().text()).info.length
        left_panel.activate_add_button(self, [
            'video',                                                                               # [0] kind
            self.left_panel_video_local_list.currentItem().thumbnail,                                                                             # [1] preview image
            self.left_panel_video_local_list.currentItem().text(),                                                                            # [2] file path
            self.left_panel_video_local_list.currentItem().thumbnail,                                                                               # [3] preview
            self.left_panel_video_local_list.currentItem().text(),                                                                               # [4] title
            0,                                                                               # [5] start
            1,                                                                                 # [6] end
            duration,                                                                            # [7] duration
            False                                                                   # [8] back to playlist
        ])
