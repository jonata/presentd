import os

from PySide6.QtCore import Qt, QCoreApplication
from PySide6.QtGui import QIcon, QPalette, QColor, QGuiApplication, QCursor, QPixmap
from PySide6.QtWidgets import QWidget, QLabel

from presentd.paths import PATH_PRESENTD_GRAPHICS
from presentd.translation import _


def load(self):
    class monitor_window(QWidget):
        def closeEvent(self, event):
            QCoreApplication.instance().quit()
            return event

    self.monitor_window = monitor_window(parent=None)
    self.monitor_window.setWindowTitle('Presentd monitor')
    self.monitor_window.setWindowFlags(Qt.Window)
    self.monitor_window.setWindowIcon(QIcon(os.path.join(PATH_PRESENTD_GRAPHICS, 'presentd.png')))

    if len(QGuiApplication.screens()) > 1:
        self.monitor_window.setWindowFlags(self.monitor_window.windowFlags() | Qt.WindowStaysOnTopHint | Qt.FramelessWindowHint | Qt.X11BypassWindowManagerHint)

        mon_scr = QGuiApplication.screens().index(QGuiApplication.screenAt(QCursor.pos())) + 1

        if mon_scr >= len(QGuiApplication.screens()):
            mon_scr = 0

        self.monitor_window.move(QGuiApplication.screens()[mon_scr].geometry().left(), QGuiApplication.screens()[mon_scr].geometry().top())
        window_width = QGuiApplication.screens()[mon_scr].geometry().width()
        window_height = QGuiApplication.screens()[mon_scr].geometry().height()

    else:
        window_width = 640
        window_height = 480
        self.monitor_window.setWindowFlags(self.monitor_window.windowFlags() | Qt.WindowStaysOnTopHint)

        self.monitor_window_warning_label = QLabel(_('monitor_window.warning_no_second_screen'), parent=self.monitor_window)
        self.monitor_window_warning_label.setGeometry(0, 0, self.monitor_window.width(), self.monitor_window.height())
        self.monitor_window_warning_label.setStyleSheet('QLabel { color: grey; font-size: 24px; }')
        self.monitor_window_warning_label.setAlignment(Qt.AlignCenter)
        self.monitor_window_warning_label.setVisible(True)

    self.monitor_window.setFixedSize(window_width, window_height)

    self.monitor_window.setCursor(Qt.BlankCursor)

    self.monitor_window.black_palette = self.palette()
    self.monitor_window.black_palette.setColor(QPalette.Window, QColor(0, 0, 0))
    self.monitor_window.setPalette(self.monitor_window.black_palette)
    self.monitor_window.setAutoFillBackground(True)
