import os

from PySide6.QtWidgets import QLabel, QWidget, QPushButton, QCheckBox, QDial, QComboBox, QLineEdit
from PySide6.QtCore import QPropertyAnimation, QEasingCurve

from presentd.paths import PATH_PRESENTD_GRAPHICS


def load(self):
    self.options_box_is_visible = False

    self.main_options_box = QLabel(parent=self)
    self.main_options_box.setObjectName('main_options_box')
    self.main_options_box.setStyleSheet('QLabel {color:silver;} QCheckBox {color:silver;} #main_options_box { border-top: 34px; border-right: 5px; border-bottom: 0; border-left: 5px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "background_options_box.png").replace('\\', '/') + '") 34 5 5 5 stretch stretch; }')
    self.main_options_box_geometry_animation = QPropertyAnimation(self.main_options_box, b'geometry')
    self.main_options_box_geometry_animation.setEasingCurve(QEasingCurve.OutCirc)

    class main_options_file_icon(QLabel):
        def mousePressEvent(widget, event):
            if not self.main_options_box_selected == 'file':
                self.main_options_box_selected = 'file'
                show_main_options(self)
            else:
                self.main_options_box_selected = False
                hide_main_options()
            widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(PATH_PRESENTD_GRAPHICS, 'main_options_file_icon_pressed.png').replace('\\', '/') + '); } ')

        def enterEvent(widget, event):
            if not self.main_options_box_selected == 'file':
                widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(PATH_PRESENTD_GRAPHICS, 'main_options_file_icon_hover.png').replace('\\', '/') + '); } ')

        def leaveEvent(widget, event):
            if not self.main_options_box_selected == 'file':
                widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(PATH_PRESENTD_GRAPHICS, 'main_options_file_icon_normal.png').replace('\\', '/') + '); } ')

    self.main_options_file_icon = main_options_file_icon(parent=self.main_options_box)
    self.main_options_file_icon.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(PATH_PRESENTD_GRAPHICS, 'main_options_file_icon_normal.png').replace('\\', '/') + '); } ')

    self.main_options_file_panel = QWidget(self.main_options_box)

    self.main_options_file_open_button = QPushButton(self.main_options_file_panel)
    self.main_options_file_open_button.setStyleSheet('QPushButton { image: url(' + os.path.join(PATH_PRESENTD_GRAPHICS, 'open_playlist_icon.png').replace('\\', '/') + '); font-family: "Ubuntu"; font-size:14px; color:white; font-weight:bold; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
    self.main_options_file_open_button.clicked.connect(lambda:self.main_options_file_open_button_clicked())

    self.main_options_file_open_label = QLabel(u'Abrir\nplaylist',self.main_options_file_panel)

    self.main_options_file_save_button = QPushButton(self.main_options_file_panel)
    self.main_options_file_save_button.setStyleSheet('QPushButton { image: url(' + os.path.join(PATH_PRESENTD_GRAPHICS, 'save_playlist_icon.png').replace('\\', '/') + '); font-family: "Ubuntu"; font-size:14px; color:white; font-weight:bold; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
    self.main_options_file_save_button.clicked.connect(lambda:self.main_options_file_save_button_clicked())

    self.main_options_file_save_label = QLabel(u'Guardar\nplaylist',self.main_options_file_panel)

    class main_options_settings_icon(QLabel):
        def mousePressEvent(widget, event):
            if not self.main_options_box_selected == 'settings':
                self.main_options_box_selected = 'settings'
                show_main_options(self)
            else:
                self.main_options_box_selected = False
                hide_main_options(self)
            widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(PATH_PRESENTD_GRAPHICS, 'main_options_settings_icon_pressed.png').replace('\\', '/') + '); } ')
        def enterEvent(widget, event):
            if not self.main_options_box_selected == 'settings':
                widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(PATH_PRESENTD_GRAPHICS, 'main_options_settings_icon_hover.png').replace('\\', '/') + '); } ')
        def leaveEvent(widget, event):
            if not self.main_options_box_selected == 'settings':
                widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(PATH_PRESENTD_GRAPHICS, 'main_options_settings_icon_normal.png').replace('\\', '/') + '); } ')

    self.main_options_settings_icon = main_options_settings_icon(parent=self.main_options_box)
    self.main_options_settings_icon.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(PATH_PRESENTD_GRAPHICS, 'main_options_settings_icon_normal.png').replace('\\', '/') + '); } ')

    self.main_options_settings_panel = QWidget(self.main_options_box)

    self.main_options_settings_enable_animations = QCheckBox(u'Animações na interface', parent=self.main_options_settings_panel)
    self.main_options_settings_enable_animations.clicked.connect(lambda:main_options_settings_enable_animations_clicked())
    self.main_options_settings_enable_animations.setChecked(self.settings.get('enable_animations', False))

    self.main_options_settings_framerate_label = QLabel(parent=self.main_options_settings_panel)

    self.main_options_settings_framerate = QDial(parent=self.main_options_settings_panel)
    self.main_options_settings_framerate.setMaximum(60)
    self.main_options_settings_framerate.setMinimum(1)
    self.main_options_settings_framerate.valueChanged.connect(lambda:main_options_settings_framerate_changing(self))
    self.main_options_settings_framerate.sliderReleased.connect(lambda:main_options_settings_framerate_changed(self))
    self.main_options_settings_framerate.setValue(self.settings.get('interface_framerate', False))

    #main_options_settings_framerate_changing(self)


    self.main_options_settings_language_label = QLabel('IDIOMA', parent=self.main_options_settings_panel)

    self.main_options_settings_language = QComboBox(parent=self.main_options_settings_panel)
    #self.main_options_settings_language.addItems(sorted([*language_list]))
    self.main_options_settings_language.activated.connect(lambda:main_options_settings_language_activated(self))
    if self.settings.get('language_mnemonic', False):
        self.main_options_settings_language.setCurrentText(dict(map(reversed, language_list.items()))[self.settings.get('language_mnemonic', False)])

    class main_options_export_icon(QLabel):
        def mousePressEvent(widget, event):
            if not self.main_options_box_selected == 'export':
                self.main_options_box_selected = 'export'
                show_main_options(self)
            else:
                self.main_options_box_selected = False
                hide_main_options(self)
            widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(PATH_PRESENTD_GRAPHICS, 'main_options_export_icon_pressed.png').replace('\\', '/') + '); } ')
        def enterEvent(widget, event):
            if not self.main_options_box_selected == 'export':
                widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(PATH_PRESENTD_GRAPHICS, 'main_options_export_icon_hover.png').replace('\\', '/') + '); } ')
        def leaveEvent(widget, event):
            if not self.main_options_box_selected == 'export':
                widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(PATH_PRESENTD_GRAPHICS, 'main_options_export_icon_normal.png').replace('\\', '/') + '); } ')

    self.main_options_export_icon = main_options_export_icon(parent=self.main_options_box)
    self.main_options_export_icon.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(PATH_PRESENTD_GRAPHICS, 'main_options_export_icon_normal.png').replace('\\', '/') + '); } ')

    self.main_options_export_panel = QWidget(self.main_options_box)

    self.main_options_export_profile = QComboBox(self.main_options_export_panel)
    #self.main_options_export_profile.addItems(['320x180 (iPod/iPhone)', '320x240 (PSP)', '720x480 (DVD player)', '1280x720 (SmartTV)', '704x480 (SmartTV)'])
    self.main_options_export_profile.addItems(['1280x720 (SmartTV)'])

    self.main_options_export_button = QPushButton(u'GERAR VÍDEO', self.main_options_export_panel)
    self.main_options_export_button.setStyleSheet('QPushButton { font-family: "Ubuntu"; font-size:14px; color:white; font-weight:bold; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }') #image: url(' + os.path.join(PATH_PRESENTD_GRAPHICS, 'main_options_export_icon_normal.png').replace('\\', '/') + ');
    self.main_options_export_button.clicked.connect(lambda:self.main_options_export_button_clicked())

    #monitor_yeartext.load_options(self)
    self.main_options_box.setVisible(False)


def resize(self):
    if self.options_box_is_visible:
        self.main_options_box.setGeometry(80, int(self.height() * .75), int((self.width() * .5) - 110), int(self.height() * .25))
    else:
        self.main_options_box.setGeometry(80, self.height() - 30, int((self.width() * .5) - 110), 30)

    self.main_options_file_icon.setGeometry(5, 0, 30, 30)
    self.main_options_file_panel.setGeometry(5, 35, int(((self.width() * .5) - 110) - 10), int((self.height() * .25) - 40))
    self.main_options_file_open_button.setGeometry(5, 5, 40, 40)
    self.main_options_file_open_label.setGeometry(55, 5, (self.main_options_file_panel.width() - 60), 40)
    self.main_options_file_save_button.setGeometry(5, 55, 40, 40)
    self.main_options_file_save_label.setGeometry(55, 55, (self.main_options_file_panel.width() - 60), 40)

    self.main_options_settings_icon.setGeometry(40, 0, 30, 30)
    self.main_options_settings_panel.setGeometry(5, 35, int(((self.width() * .5) - 110) - 10), int((self.height() * .25) - 40))
    self.main_options_settings_enable_animations.setGeometry(5, 5, self.main_options_settings_panel.width() - 10, 20)
    self.main_options_settings_framerate.setGeometry(5, 35, 40, 40)
    self.main_options_settings_framerate_label.setGeometry(50, 35, self.main_options_settings_panel.width() - 55, 40)
    self.main_options_settings_language_label.setGeometry(5, 85, self.main_options_settings_panel.width() - 55, 20)
    self.main_options_settings_language.setGeometry(5, 105, self.main_options_settings_panel.width() - 10, 30)

    self.main_options_export_icon.setGeometry(75, 0, 30, 30)
    self.main_options_export_panel.setGeometry(5, 35, int(((self.width() * .5) - 110) - 10), int((self.height() * .25) - 40))
    self.main_options_export_profile.setGeometry(5, 5, self.main_options_export_panel.width() - 10, 20)
    self.main_options_export_button.setGeometry(5, 35, self.main_options_export_panel.width() - 10, 40)

    #monitor_yeartext.resize_options(self)
