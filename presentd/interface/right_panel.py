from PySide6.QtWidgets import QLabel, QWidget, QGraphicsOpacityEffect, QPushButton, QHBoxLayout, QVBoxLayout
from PySide6.QtGui import QPainter, QImage
from PySide6.QtCore import Qt, QPropertyAnimation, Signal, QRect, QMargins

from presentd.utils import generate_effect
from presentd.translation import _
from presentd.interface import playlist_panel


class right_panel_monitor(QLabel):
    hide_all_pressed = Signal()

    def __init__(widget, height_ratio=1):
        super().__init__()
        widget.height_ratio = height_ratio
        widget.main_background_qimage = False

        widget.setObjectName("right_panel_monitor")
        widget.setLayout(QVBoxLayout())
        widget.layout().setContentsMargins(0, 0, 0, 0)

        widget.top_bar = QWidget()
        widget.top_bar.setObjectName('right_panel_monitor_top_bar')
        widget.top_bar.setLayout(QHBoxLayout())
        widget.top_bar.layout().setContentsMargins(5, 5, 5, 5)

        widget.top_bar_title = QLabel()
        widget.top_bar_title.setObjectName('right_panel_monitor_top_bar_title')
        widget.top_bar.layout().addWidget(widget.top_bar_title)

        widget.top_bar_type = QLabel()
        widget.top_bar_type.setObjectName('right_panel_monitor_top_bar_type')
        widget.top_bar.layout().addWidget(widget.top_bar_type, 0, Qt.AlignRight)

        widget.layout().addWidget(widget.top_bar, 0, Qt.AlignTop)

        widget.hide_all_button = QPushButton()
        widget.hide_all_button.setObjectName('right_panel_monitor_hide_all_button')
        widget.hide_all_button.setProperty('class', 'red')
        widget.hide_all_button.clicked.connect(lambda: widget.hide_all_pressed.emit())
        widget.hide_all_button_opacity = QGraphicsOpacityEffect()
        widget.hide_all_button.setGraphicsEffect(widget.hide_all_button_opacity)
        widget.hide_all_button_opacity.setOpacity(0.0)
        widget.hide_all_button_opacity_animation = QPropertyAnimation(widget.hide_all_button_opacity, b'opacity')

        widget.layout().addWidget(widget.hide_all_button, 1, Qt.AlignCenter)

    def resizeEvent(widget, event):
        widget.setFixedHeight((widget.width() * widget.height_ratio) + 8)
        return event

    def enterEvent(widget, event):
        generate_effect(widget.hide_all_button_opacity_animation, 'opacity', 200, widget.hide_all_button_opacity.opacity(), 1.0)
        return event

    def leaveEvent(widget, event):
        generate_effect(widget.hide_all_button_opacity_animation, 'opacity', 200, widget.hide_all_button_opacity.opacity(), 0.0)
        return event

    def paintEvent(widget, event):
        painter = QPainter(widget)
        x = 24
        y = 24
        full_rect = QRect(0, 0, widget.width(), widget.height()) - QMargins(x, y, x, y)

        if widget.main_background_qimage is not False:
            qimage = QImage(widget.main_background_qimage).scaled(widget.width() - (x * 2), widget.height() - (y * 2), Qt.KeepAspectRatio)
            painter.drawImage(full_rect, qimage)

        painter.end()

        return event


class right_panel_preview(QLabel):
    def __init__(widget, height_ratio=1):
        super().__init__()
        widget.height_ratio = height_ratio
        widget.main_background_qpixmap = False

        widget.setObjectName("right_panel_preview")
        widget.setLayout(QVBoxLayout())
        widget.layout().setContentsMargins(0, 0, 0, 0)

        widget.bottom_bar = QWidget()
        widget.bottom_bar.setObjectName('right_panel_preview_bottom_bar')
        widget.bottom_bar.setLayout(QHBoxLayout())
        widget.bottom_bar.layout().setContentsMargins(5, 5, 5, 5)

        widget.bottom_bar_title = QLabel()
        widget.bottom_bar_title.setObjectName('right_panel_preview_bottom_bar_title')
        widget.bottom_bar.layout().addWidget(widget.bottom_bar_title)

        widget.bottom_bar_type = QLabel()
        widget.bottom_bar_type.setObjectName('right_panel_preview_bottom_bar_type')
        widget.bottom_bar.layout().addWidget(widget.bottom_bar_type, 0, Qt.AlignRight)

        widget.layout().addWidget(widget.bottom_bar, 0, Qt.AlignBottom)

    def resizeEvent(widget, event):
        widget.setFixedHeight(widget.width() * widget.height_ratio)
        return event

    def paintEvent(widget, event):
        painter = QPainter(widget)
        x = 24
        y = 24
        full_rect = QRect(0, 0, widget.width(), widget.height()) - QMargins(x, y, x, y)

        if widget.main_background_qpixmap:
            qpixmap = widget.main_background_qpixmap.scaled(widget.width() - (x * 2), widget.height() - (y * 2), Qt.KeepAspectRatio, Qt.SmoothTransformation)
            painter.drawPixmap(full_rect, qpixmap)

        painter.end()

        return event


def load(self):
    # "GLOBAL" variables
    self.actual_media = {}

    self.right_panel = QWidget()
    self.right_panel.setObjectName('right_panel')
    self.right_panel.setLayout(QVBoxLayout())
    self.right_panel.layout().setContentsMargins(0, 0, 0, 0)
    self.right_panel.layout().setSpacing(0)

    height_ratio = float(self.monitor_window.height()) / float(self.monitor_window.width())
    self.right_panel_monitor = right_panel_monitor(height_ratio=height_ratio)
    self.right_panel_monitor.hide_all_pressed.connect(lambda: hide_all_pressed(self))
    self.right_panel.layout().addWidget(self.right_panel_monitor, 0, Qt.AlignTop)

    self.right_panel_properties = QWidget()
    self.right_panel_properties.setLayout(QVBoxLayout())
    self.right_panel_properties.layout().setContentsMargins(20, 0, 20, 0)
    self.right_panel.layout().addWidget(self.right_panel_properties)

    self.right_panel.layout().addStretch()

    self.right_panel_shownext_button = QPushButton()
    self.right_panel_shownext_button.setFixedHeight(72)
    self.right_panel_shownext_button.setObjectName('right_panel_shownext_button')
    self.right_panel_shownext_button.setLayout(QVBoxLayout())
    self.right_panel_shownext_button.layout().setContentsMargins(0, 8, 0, 0)
    self.right_panel_shownext_button.clicked.connect(lambda: right_panel_shownext_button_clicked(self))
    self.right_panel.layout().addWidget(self.right_panel_shownext_button)

    self.right_panel_shownext_button_label = QLabel()
    self.right_panel_shownext_button_label.setObjectName('right_panel_shownext_button_label')
    self.right_panel_shownext_button.layout().addWidget(self.right_panel_shownext_button_label, 0, Qt.AlignCenter)

    self.right_panel_shownext_preview_background = QWidget()
    self.right_panel_shownext_preview_background.setObjectName('right_panel_shownext_preview_background')
    self.right_panel_shownext_preview_background.setLayout(QHBoxLayout())
    self.right_panel_shownext_preview_background.layout().setContentsMargins(0, 0, 0, 0)
    self.right_panel.layout().addWidget(self.right_panel_shownext_preview_background)

    self.right_panel_preview = right_panel_preview(height_ratio=height_ratio)
    self.right_panel_shownext_preview_background.layout().addWidget(self.right_panel_preview)

    # self.right_panel_shownext_preview_title = QLabel(u'NEXT MEDIA', parent=self.right_panel_shownext_preview_background)
    # self.right_panel_shownext_preview_title.setObjectName('right_panel_shownext_preview_title')
    # self.right_panel_shownext_preview_title.setStyleSheet('#right_panel_shownext_preview_title { padding-left:3px; padding-top:4px; font-family: "Ubuntu"; font-size:11px; font-weight:bold; color:white; border-bottom: 3px; border-right: 3px; border-left: 3px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "shownext_preview_title_background.png").replace('\\', '/') + '") 0 3 3 3 stretch stretch; }')

    # self.right_panel_shownext_preview = QWidget(parent=self.right_panel_shownext_button)
    # self.right_panel_shownext_preview_black_palette = self.right_panel_shownext_preview.palette()
    # self.right_panel_shownext_preview_black_palette.setColor(QPalette.Window, QColor(0,0,0))
    # self.right_panel_shownext_preview.setPalette(self.right_panel_shownext_preview_black_palette)
    # self.right_panel_shownext_preview.setAutoFillBackground(True)

    self.main_widget.layout().addWidget(self.right_panel, 0, 3, 0, 1)

    update_widgets(self)


def translate(self):
    self.right_panel_monitor.top_bar_title.setText(_('right_panel.showing_now'))
    self.right_panel_monitor.hide_all_button.setText(_('right_panel.hide_all'))
    self.right_panel_preview.bottom_bar_title.setText(_('right_panel.next_media'))


def hide_all_pressed(self):
    if self.actual_media:
        self.actual_media['hide_media_function']()


def right_panel_shownext_button_clicked(self):
    hide_all_pressed(self)
    self.actual_media = self.playlist.pop(0)
    self.actual_media['show_media_function']()
    playlist_panel.update_widgets(self)
    update_widgets(self)


def update_widgets(self):
    self.right_panel_shownext_button.setEnabled(bool(self.playlist))
    self.right_panel_shownext_button_label.setEnabled(bool(self.playlist))
    self.right_panel_shownext_button_label.setText(_('right_panel.show_next_media') if self.playlist else _('right_panel.no_media_to_show'))

    self.right_panel_monitor.top_bar_type.setVisible(bool(self.actual_media))
    self.right_panel_monitor.top_bar_type.setText(_('right_panel.media_type.{}'.format(self.actual_media.get('type', 'media'))))

    self.right_panel_preview.bottom_bar_type.setVisible(bool(self.playlist))
    self.right_panel_preview.bottom_bar_type.setText(_('right_panel.media_type.{}'.format(self.playlist[0].get('type', 'media') if self.playlist else 'media')))

    self.right_panel_preview.main_background_qpixmap = self.playlist[0].get('thumbnail', None) if self.playlist else None
    self.right_panel_preview.update()

#     self.right_panel_shownext_button = QLabel(parent=self.right_panel_widget)
#     self.right_panel_shownext_button.setObjectName('right_panel_shownext_button')
#     self.right_panel_shownext_button.setStyleSheet('#right_panel_shownext_button { border-top: 75px; border-right: 25px; border-left: 25px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "shownext_box_background.png").replace('\\', '/') + '") 75 25 0 25 stretch stretch; }')

#     self.right_panel_shownext_preview_background = QLabel(parent=self.right_panel_shownext_button)
#     self.right_panel_shownext_preview_background.setObjectName('right_panel_shownext_preview_background')
#     self.right_panel_shownext_preview_background.setStyleSheet('#right_panel_shownext_preview_background { border-top: 4px; border-right: 4px; border-bottom: 4px; border-left: 4px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "black_box.png").replace('\\', '/') + '") 4 4 4 4 stretch stretch; }')

#     self.right_panel_shownext_preview_title = QLabel(u'NEXT MEDIA', parent=self.right_panel_shownext_preview_background)
#     self.right_panel_shownext_preview_title.setObjectName('right_panel_shownext_preview_title')
#     self.right_panel_shownext_preview_title.setStyleSheet('#right_panel_shownext_preview_title { padding-left:3px; padding-top:4px; font-family: "Ubuntu"; font-size:11px; font-weight:bold; color:white; border-bottom: 3px; border-right: 3px; border-left: 3px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "shownext_preview_title_background.png").replace('\\', '/') + '") 0 3 3 3 stretch stretch; }')

#     self.right_panel_shownext_preview = QWidget(parent=self.right_panel_shownext_button)
#     self.right_panel_shownext_preview_black_palette = self.right_panel_shownext_preview.palette()
#     self.right_panel_shownext_preview_black_palette.setColor(QPalette.Window, QColor(0,0,0))
#     self.right_panel_shownext_preview.setPalette(self.right_panel_shownext_preview_black_palette)
#     self.right_panel_shownext_preview.setAutoFillBackground(True)

#     self.right_panel_shownext_preview_audio = QWidget(parent=self.right_panel_shownext_preview)
#     self.right_panel_shownext_preview_audio_artwork = QLabel(parent=self.right_panel_shownext_preview_audio)
#     self.right_panel_shownext_preview_audio_artwork.setAlignment(Qt.AlignCenter)
#     self.right_panel_shownext_preview_audio_title = QLabel(parent=self.right_panel_shownext_preview_audio)
#     self.right_panel_shownext_preview_audio_title.setAlignment(Qt.AlignVCenter | Qt.AlignLeft)
#     self.right_panel_shownext_preview_audio_title.setWordWrap(True)

#     self.right_panel_shownext_preview_image = QLabel(parent=self.right_panel_shownext_preview)
#     self.right_panel_shownext_preview_image.setAlignment(Qt.AlignCenter)

#     self.right_panel_shownext_preview_clock = QLabel(parent=self.right_panel_shownext_preview)
#     self.right_panel_shownext_preview_clock.setAlignment(Qt.AlignCenter)

#     class right_panel_shownext_button(QLabel):
#         def mouseReleaseEvent(widget, event):
#             if len(self.playlist) > 0:
#                 self.right_panel_shownext_button.setStyleSheet('#right_panel_shownext_button { border-top: 75px; border-right: 25px; border-left: 25px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "shownext_box_background_hover.png").replace('\\', '/') + '") 75 25 0 25 stretch stretch; }')
#             else:
#                 self.right_panel_shownext_button.setStyleSheet('#right_panel_shownext_button { border-top: 75px; border-right: 25px; border-left: 25px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "shownext_box_background_disabled.png").replace('\\', '/') + '") 75 25 0 25 stretch stretch; }')
#         def mousePressEvent(widget, event):
#             if len(self.playlist) > 0:
#                 show_now_clicked(self)
#                 self.right_panel_shownext_button.setStyleSheet('#right_panel_shownext_button { border-top: 75px; border-right: 25px; border-left: 25px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "shownext_box_background_pressed.png").replace('\\', '/') + '") 75 25 0 25 stretch stretch; }')
#         def enterEvent(widget, event):
#             if len(self.playlist) > 0:
#                 self.right_panel_shownext_button.setStyleSheet('#right_panel_shownext_button { border-top: 75px; border-right: 25px; border-left: 25px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "shownext_box_background_hover.png").replace('\\', '/') + '") 75 25 0 25 stretch stretch; }')
#         def leaveEvent(widget, event):
#             if len(self.playlist) > 0:
#                 self.right_panel_shownext_button.setStyleSheet('#right_panel_shownext_button { border-top: 75px; border-right: 25px; border-left: 25px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "shownext_box_background.png").replace('\\', '/') + '") 75 25 0 25 stretch stretch; }')
#             else:
#                 self.right_panel_shownext_button.setStyleSheet('#right_panel_shownext_button { border-top: 75px; border-right: 25px; border-left: 25px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "shownext_box_background_disabled.png").replace('\\', '/') + '") 75 25 0 25 stretch stretch; }')

#     self.right_panel_shownext_button = right_panel_shownext_button(parent=self.right_panel_shownext_button)
#     self.right_panel_shownext_button.setScaledContents(True)
#     self.right_panel_shownext_button.setStyleSheet('QLabel {font-family: "Ubuntu"; font-size:12pt; font-weight:bold; color:white; qproperty-alignment: "AlignCenter";}')


# def resize(self):
#     self.right_panel_widget.setGeometry(int(self.width() * .75), 0, int(self.width() * .25), self.height())
#     self.right_panel_monitor_background.setGeometry(20, 20, self.right_panel_widget.width() - 40, int(((self.right_panel_widget.width()-40) * self.screen_width_proportion) + 25))
#     self.right_panel_monitor_title.setGeometry(3, 3, self.right_panel_monitor_background.width() - 6, 25)
#     self.right_panel_monitor.setGeometry(3, 28, self.right_panel_monitor_background.width() - 6, int((self.right_panel_widget.width() - 46) * self.screen_width_proportion))
#     self.right_panel_monitor_withdrawbutton.setGeometry(int(self.right_panel_monitor.width() * .3), int(self.right_panel_monitor.height() * .4), int(self.right_panel_monitor.width() * .4), int(self.right_panel_monitor.height() * .2))
#     self.right_panel_monitor_player.setGeometry(20, 20 + self.right_panel_monitor_background.height() + 10, self.right_panel_monitor_background.width(), 40)
#     self.right_panel_monitor_player_stopbutton.setGeometry(0,0,self.right_panel_monitor_player.height(),self.right_panel_monitor_player.height())
#     self.right_panel_monitor_player_playbutton.setGeometry(self.right_panel_monitor_player.height()+1,0,self.right_panel_monitor_player.height(),self.right_panel_monitor_player.height())
#     self.right_panel_monitor_player_timelinebox.setGeometry((self.right_panel_monitor_player.height()*2)+2,0,self.right_panel_monitor_player.width()-((self.right_panel_monitor_player.height()*2)+2),self.right_panel_monitor_player.height())
#     self.right_panel_monitor_player_timeline.setGeometry(0,3,self.right_panel_monitor_player_timelinebox.width()-3,self.right_panel_monitor_player_timelinebox.height()-6)
#     self.right_panel_shownext_button.setGeometry(0, int(self.right_panel_widget.height() - 80 - ((self.right_panel_widget.width() - 46) * self.screen_width_proportion) - 25 - 26), self.right_panel_widget.width(), int(80 + ((self.right_panel_widget.width() - 46) * self.screen_width_proportion) + 25 + 26))
#     self.right_panel_shownext_preview_background.setGeometry(20, 80, self.right_panel_widget.width() - 40, int(((self.right_panel_widget.width() - 40) * self.screen_width_proportion) + 25))
#     self.right_panel_shownext_preview_title.setGeometry(3, self.right_panel_monitor_background.height() - 28, self.right_panel_monitor_background.width() - 6, 25)
#     self.right_panel_shownext_preview.setGeometry(23, 83, self.right_panel_monitor_background.width() - 6, int((self.right_panel_widget.width()-46)*self.screen_width_proportion))
#     self.right_panel_shownext_preview_audio.setGeometry(0, 0, self.right_panel_shownext_preview.width(), self.right_panel_shownext_preview.height())
#     self.right_panel_shownext_preview_audio_artwork.setGeometry(int(self.right_panel_shownext_preview.width()*.1), int((self.right_panel_shownext_preview.height() * .5) - (self.right_panel_shownext_preview.height() * .1)), int(self.right_panel_shownext_preview.height() * .2), int(self.right_panel_shownext_preview.height() * .2))
#     self.right_panel_shownext_preview_audio_title.setGeometry(int((self.right_panel_shownext_preview.width() * .11) + (self.right_panel_shownext_preview.width() * .2)), 0, int(self.right_panel_shownext_preview.width() - ((self.right_panel_shownext_preview.width() * .21) + (self.right_panel_shownext_preview.width() * .2))), self.right_panel_shownext_preview.height())
#     self.right_panel_shownext_preview_audio_artwork.setStyleSheet('QLabel {background-color: #fff; font-family: "Ubuntu Condensed"; font-size:' + str(self.right_panel_shownext_preview.height()*.09) + 'pt; font-weight:bold; color:black;}')
#     self.right_panel_shownext_preview_audio_title.setStyleSheet('QLabel {background-color:none; font-family: "Ubuntu"; font-size:' + str(self.right_panel_shownext_preview.height()*.05) + 'pt; font-weight:bold; color:white}')
#     self.right_panel_shownext_preview_image.setGeometry(0,0,self.right_panel_shownext_preview.width(), self.right_panel_shownext_preview.height())
#     self.right_panel_shownext_preview_clock.setGeometry(0,0,self.right_panel_shownext_preview.width(), self.right_panel_shownext_preview.height())
#     self.right_panel_shownext_preview_clock.setStyleSheet('QLabel {background-color: black; font-family: "Ubuntu"; font-size:' + str(self.right_panel_shownext_preview.height()*.1) + 'pt; font-weight:bold; color:white}')
#     self.right_panel_shownext_button.setGeometry(0,0,self.right_panel_shownext_button.width(),80)


# def update_preview(self):
#     if len(self.playlist) > 0:

#         self.selected_media = self.playlist[0]
#         self.right_panel_shownext_preview.setVisible(True)
#         if not self.selected_media[0] == 'audio':
#             self.right_panel_shownext_preview_audio.setVisible(False)
#         if not self.selected_media[0] in ['image', 'video', 'web', 'screencopy','camera']:
#             self.right_panel_shownext_preview_image.setPixmap(None)
#         if not self.selected_media[0] in ['clock']:
#             self.right_panel_shownext_preview_clock.setVisible(False)
#         if not self.selected_media[0] in ['camera']:
#             None #TODO

#         if self.selected_media[0] == 'audio':
#             self.right_panel_shownext_preview_audio_artwork.setText(str(self.selected_media[2]))
#             self.right_panel_shownext_preview_audio_title.setText(self.selected_media[3])
#             self.right_panel_shownext_preview_audio.setVisible(True)
#         elif self.selected_media[0] == 'image':
#             self.right_panel_shownext_preview_image.setPixmap(QPixmap(self.selected_media[2]).scaled(self.right_panel_shownext_preview_image.width(), self.right_panel_shownext_preview_image.height(), Qt.KeepAspectRatio, Qt.SmoothTransformation))
#         elif self.selected_media[0] == 'video':
#             self.right_panel_shownext_preview_image.setPixmap(self.selected_media[3].scaled(self.right_panel_shownext_preview_image.width(), self.right_panel_shownext_preview_image.height(), Qt.KeepAspectRatio, Qt.SmoothTransformation))

#         elif self.selected_media[0] == 'web':
#             self.right_panel_shownext_preview_image.setPixmap(self.selected_media[3].scaled(self.right_panel_shownext_preview_image.width(), self.right_panel_shownext_preview_image.height(), Qt.KeepAspectRatio, Qt.SmoothTransformation))
#         elif self.selected_media[0] == 'clock':
#             self.right_panel_shownext_preview_clock.setVisible(True)
#         elif self.selected_media[0] == 'screencopy':
#             None
#         elif self.selected_media[0] == 'camera':
#             None #TODO

#     else:
#         self.right_panel_shownext_preview.setVisible(False)

#         #self.right_panel_shownext_preview


# def show_now_clicked(self):
#     monitor_window.show_media(self, self.playlist[0])

#     if self.playlist[0][-1]:
#         self.playlist.append(self.playlist.pop(0))
#     else:
#         del self.playlist[0]

#     self.playlist_options_is_visible = False

#     playlist_panel.playlist_options_panel_open_button_clicked(self)

#     playlist_panel.populate_playlist(self)

#     update_preview(self)


# def right_panel_monitor_player_stopbutton_pressed(self):
#     self.monitor_window_player.stop()
#     self.right_panel_monitor_player_playbutton.setChecked(False)


# def right_panel_monitor_player_playpause_pressed(self):
#     self.monitor_window_player.pause()

