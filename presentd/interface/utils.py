
def convert_to_timecode(value, simple=False):
    fr = int(str('%.3f'% value).split('.')[-1])
    mm, ss = divmod(value, 60)
    hh, mm = divmod(mm, 60)

    if simple:
        if hh == 0 and mm == 0:
            return '%02d' % (ss)
        elif hh == 0:
            return '%02d:%02d' % (mm, ss)
        else:
            return '%02d:%02d:%02d' % (hh, mm, ss)
    else:
        return '%02d:%02d:%02d.%03d' % (hh, mm, ss, fr)