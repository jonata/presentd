import os
import ctypes
import platform
from mpv import MPV, MpvRenderContext, MpvGlGetProcAddressFn

from PySide6.QtWidgets import QListWidget, QPushButton, QWidget, QVBoxLayout, QFileDialog, QTabWidget, QHBoxLayout, QListView, QLabel, QListWidgetItem, QSizePolicy, QGraphicsOpacityEffect
from PySide6.QtCore import Signal, Qt, QSize, QThread, QRectF, QMargins, QPropertyAnimation
from PySide6.QtGui import QIcon, QPixmap, QPainter, QFont, QColor
from PySide6.QtOpenGLWidgets import QOpenGLWidget

from presentd.interface.left_panel import left_panel_addition_menu_button
from presentd.interface import playlist_panel
from presentd.translation import _
from presentd.paths import REAL_PATH_HOME
from presentd.utils import get_thumbnail, convert_to_timecode, generate_effect


class thumbnail_generate_thread(QThread):
    response = Signal(dict)
    list_of_filepahts = []
    size = QSize(40, 30)

    def run(thread):
        if thread.list_of_filepahts:
            final_response_dict = {}
            for filepath in thread.list_of_filepahts:
                final_response_dict[filepath] = get_thumbnail(thread.size, filepath=filepath, media_type='image')

            thread.response.emit(final_response_dict)


image_thumbnail_generate_thread = thumbnail_generate_thread()


def load(self):
    self.monitor_window_image_qlabel = QLabel(parent=self.monitor_window)
    self.monitor_window_image_qlabel.setVisible(False)
    self.monitor_window_image_qlabel.setGeometry(0, 0, self.monitor_window.width(), self.monitor_window.height())
    self.monitor_window_image_qlabel.setAlignment(Qt.AlignCenter)

    self.monitor_window_image_qlabel_opacity = QGraphicsOpacityEffect()
    self.monitor_window_image_qlabel.setGraphicsEffect(self.monitor_window_image_qlabel_opacity)
    self.monitor_window_image_qlabel_opacity_animation = QPropertyAnimation(self.monitor_window_image_qlabel_opacity, b'opacity')
    self.monitor_window_image_qlabel_opacity.setOpacity(0.0)


    self.left_panel_widget_image_adddition_panel = QWidget()
    self.left_panel_widget_image_adddition_panel.setLayout(QVBoxLayout())

    self.left_panel_widget_image_addition_panel_tabwidget = QTabWidget()

    self.left_panel_widget_image_addition_panel_bookmarked_panel = QWidget()
    self.left_panel_widget_image_addition_panel_bookmarked_panel.setLayout(QVBoxLayout())
    self.left_panel_widget_image_addition_panel_bookmarked_panel.layout().setContentsMargins(0, 0, 0, 0)
    self.left_panel_widget_image_addition_panel_bookmarked_panel.layout().setSpacing(0)

    class left_panel_widget_image_addition_panel_bookmarked_qlistwidget(QListWidget):
        def __init__(widget, parent=None):
            super().__init__(parent)
            widget.list = []
            height_ratio = float(self.monitor_window.height()) / float(self.monitor_window.width())
            widget.icon_size = QSize(64, 64 * height_ratio)
            widget.setIconSize(widget.icon_size)

        def update(widget):
            widget.clear()
            for item in widget.list:
                item_widget = QListWidgetItem(item.get('icon', QIcon(get_thumbnail(widget.icon_size, media_type='image'))), item['title'])
                widget.addItem(item_widget)

        def add_item(widget, item):
            widget.list.append(item)
            save_bookmarked_files(self)
            widget.update()

        def remove_item(widget, item):
            del widget.list[item]
            save_bookmarked_files(self)
            widget.update()

        def leaveEvent(widget, event):
            self.left_panel_widget_image_addition_panel_buttons_line.setVisible(False)
            return event

        def enterEvent(widget, event):
            self.left_panel_widget_image_addition_panel_buttons_line.setVisible(True)
            return event

    self.left_panel_widget_image_addition_panel_bookmarked_qlistwidget = left_panel_widget_image_addition_panel_bookmarked_qlistwidget()
    self.left_panel_widget_image_addition_panel_bookmarked_qlistwidget.setViewMode(QListView.IconMode)
    self.left_panel_widget_image_addition_panel_bookmarked_qlistwidget.currentItemChanged.connect(lambda: left_panel_widget_image_addition_panel_bookmarked_qlistwidget_currentItemChanged(self))
    self.left_panel_widget_image_addition_panel_bookmarked_qlistwidget.setLayout(QVBoxLayout())
    self.left_panel_widget_image_addition_panel_bookmarked_qlistwidget.layout().addStretch()

    self.left_panel_widget_image_addition_panel_buttons_line = QWidget()
    self.left_panel_widget_image_addition_panel_buttons_line.setLayout(QHBoxLayout())
    self.left_panel_widget_image_addition_panel_buttons_line.layout().setContentsMargins(0, 0, 0, 0)
    self.left_panel_widget_image_addition_panel_buttons_line.layout().addStretch()

    self.left_panel_widget_image_addition_panel_add_button = QPushButton()
    self.left_panel_widget_image_addition_panel_add_button.setObjectName('left_panel_widget_image_addition_panel_add_button')
    self.left_panel_widget_image_addition_panel_add_button.clicked.connect(lambda: left_panel_widget_image_addition_panel_add_button_clicked(self))
    self.left_panel_widget_image_addition_panel_buttons_line.layout().addWidget(self.left_panel_widget_image_addition_panel_add_button)

    self.left_panel_widget_image_addition_panel_remove_button = QPushButton()
    self.left_panel_widget_image_addition_panel_remove_button.setObjectName('left_panel_widget_image_addition_panel_remove_button')
    self.left_panel_widget_image_addition_panel_remove_button.setVisible(False)
    self.left_panel_widget_image_addition_panel_remove_button.clicked.connect(lambda: left_panel_widget_image_addition_panel_remove_button_clicked(self))
    self.left_panel_widget_image_addition_panel_buttons_line.layout().addWidget(self.left_panel_widget_image_addition_panel_remove_button)

    self.left_panel_widget_image_addition_panel_bookmarked_qlistwidget.layout().addWidget(self.left_panel_widget_image_addition_panel_buttons_line, 0)

    self.left_panel_widget_image_addition_panel_bookmarked_panel.layout().addWidget(self.left_panel_widget_image_addition_panel_bookmarked_qlistwidget)

    self.left_panel_widget_image_addition_panel_tabwidget.addTab(self.left_panel_widget_image_addition_panel_bookmarked_panel, '')

    self.left_panel_widget_image_adddition_panel.layout().addWidget(self.left_panel_widget_image_addition_panel_tabwidget)

    self.left_panel_widget.stacked_widget.addWidget(self.left_panel_widget_image_adddition_panel)

    self.left_panel_widget_image_addition_menu_button = left_panel_addition_menu_button()
    self.left_panel_widget_image_addition_menu_button.setObjectName('left_panel_widget_image_addition_menu_button')
    self.left_panel_widget_image_addition_menu_button.setProperty('type', 'image')
    self.left_panel_widget_image_addition_menu_button.clicked.connect(lambda: addition_selected(self))
    self.left_panel_widget.vertical_menu.layout().addWidget(self.left_panel_widget_image_addition_menu_button)

    def thumbnail_generate_thread_response_received(response):
        for item in self.left_panel_widget_image_addition_panel_bookmarked_qlistwidget.list:
            for image_file in response:
                if item.get('filepath', False) and item['filepath'] == image_file:
                    item['thumbnail'] = response[image_file]
                    item['icon'] = QIcon(item['thumbnail'])
        self.left_panel_widget_image_addition_panel_bookmarked_qlistwidget.update()

    image_thumbnail_generate_thread.response.connect(thumbnail_generate_thread_response_received)
    load_bookmarked_files(self)


def show_media(self, addition):
    self.monitor_window_image_qlabel.setVisible(True)
    self.monitor_window_image_qlabel.setPixmap(QPixmap(addition['media']).scaled(self.monitor_window_image_qlabel.width(), self.monitor_window_image_qlabel.height(), Qt.KeepAspectRatio, Qt.SmoothTransformation))
    generate_effect(self.monitor_window_image_qlabel_opacity_animation, 'opacity', 1000, self.monitor_window_image_qlabel_opacity.opacity(), 1.0)


def hide_media(self):
    self.monitor_window_image_qlabel.setVisible(False)
    generate_effect(self.monitor_window_image_qlabel_opacity_animation, 'opacity', 1000, self.monitor_window_image_qlabel_opacity.opacity(), 0.0)


def translate(self):
    self.left_panel_widget_image_addition_panel_tabwidget.setTabText(0, _('modules.image.bookmarked'))
    # self.left_panel_widget_image_addition_panel_add_button.setText(_('modules.image.add_image'))
    # self.left_panel_widget_image_addition_panel_remove_button.setText(_('modules.image.remove_image'))


def left_panel_widget_image_addition_panel_remove_button_clicked(self):
    if self.left_panel_widget_image_addition_panel_bookmarked_qlistwidget.currentItem():
        self.left_panel_widget_image_addition_panel_bookmarked_qlistwidget.remove_item(self.left_panel_widget_image_addition_panel_bookmarked_qlistwidget.currentRow())


def left_panel_widget_image_addition_panel_add_button_clicked(self):
    image_path_list = QFileDialog.getOpenFileNames(self, 'Select the media files to add', REAL_PATH_HOME, "Image files (*.png *.jpg *.jpeg *.pdf)")[0]
    for filename in image_path_list:
        if os.path.isfile(filename):
            item = {
                'title': os.path.basename(filename),
                'filepath': filename,
                'thumbnail': get_thumbnail(self.left_panel_widget_image_addition_panel_bookmarked_qlistwidget.icon_size, media_type='image')
            }
            self.left_panel_widget_image_addition_panel_bookmarked_qlistwidget.add_item(item)

    filepath_list = []
    for item in self.left_panel_widget_image_addition_panel_bookmarked_qlistwidget.list:
        if not item.get('thumbnail', False):
            filepath_list.append(item['filepath'])

    image_thumbnail_generate_thread.list_of_filepahts = filepath_list
    image_thumbnail_generate_thread.size = self.right_panel_preview.rect().size()
    image_thumbnail_generate_thread.start()


def left_panel_widget_image_addition_panel_bookmarked_qlistwidget_currentItemChanged(self):
    if self.left_panel_widget_image_addition_panel_bookmarked_qlistwidget.currentItem():
        self.left_panel_addbutton.show()
    self.left_panel_widget_image_addition_panel_remove_button.setVisible(bool(self.left_panel_widget_image_addition_panel_bookmarked_qlistwidget.currentItem()))


def addition_selected(self):
    addition = {
        'type': 'image',
        'show_widgets_function': lambda: show_addition_widgets(self),
        'add_to_playlist_function': lambda: add_image_to_playlist(self),
    }
    self.left_panel_widget.addition_clicked(addition)


def show_addition_widgets(self):
    self.left_panel_widget.stacked_widget.setCurrentWidget(self.left_panel_widget_image_adddition_panel)


def add_image_to_playlist(self):
    media = self.left_panel_widget_image_addition_panel_bookmarked_qlistwidget.list[self.left_panel_widget_image_addition_panel_bookmarked_qlistwidget.currentRow()]
    # image_mp4_piece = MP4(media['filepath'])

    title = media.get('title', '')
    # if '\xa9nam' in  [*image_mp4_piece.tags]:
    #     title = image_mp4_piece.tags['\xa9nam'][0]
    # if '\xa9alb' in  [*image_mp4_piece.tags]:
    #     album = image_mp4_piece.tags['\xa9alb'][0]

    addition = {
        'type': 'image',
        'media': media.get('filepath', ''),
        'title': title,
        'hide_media_function': lambda: hide_media(self),
        'thumbnail': media.get('thumbnail', ''),
        # 'duration': image_mp4_piece.info.length
    }
    addition['show_media_function'] = lambda: show_media(self, addition)

    playlist_panel.add_media_to_playlist(self, addition)


def load_bookmarked_files(self):
    self.left_panel_widget_image_addition_panel_bookmarked_qlistwidget.list = []
    filepath_list = []
    for filepath in self.settings.get('image_bookmarked_files', []):
        if os.path.isfile(filepath):
            item = {
                'title': os.path.basename(filepath),
                'filepath': filepath,
                'thumbnail': get_thumbnail(self.left_panel_widget_image_addition_panel_bookmarked_qlistwidget.icon_size, media_type='image')
            }
            self.left_panel_widget_image_addition_panel_bookmarked_qlistwidget.add_item(item)
            filepath_list.append(filepath)
    image_thumbnail_generate_thread.list_of_filepahts = filepath_list
    image_thumbnail_generate_thread.size = self.right_panel_preview.rect().size()
    image_thumbnail_generate_thread.start()


def save_bookmarked_files(self):
    self.settings['image_bookmarked_files'] = [item.get('filepath', '') for item in self.left_panel_widget_image_addition_panel_bookmarked_qlistwidget.list]
