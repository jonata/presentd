import os
import ctypes
import platform
from mpv import MPV, MpvRenderContext, MpvGlGetProcAddressFn

from PySide6.QtWidgets import QListWidget, QPushButton, QWidget, QVBoxLayout, QFileDialog, QTabWidget, QHBoxLayout, QListView, QLabel, QListWidgetItem, QSizePolicy
from PySide6.QtCore import Signal, Qt, QSize, QRectF, QMargins, QRunnable, QObject
from PySide6.QtGui import QIcon, QPainter, QFont, QColor
from PySide6.QtOpenGLWidgets import QOpenGLWidget

from presentd.interface.left_panel import left_panel_addition_menu_button
from presentd.interface import playlist_panel
from presentd.translation import _
from presentd.paths import REAL_PATH_HOME
from presentd.utils import get_thumbnail, convert_to_timecode


class MyEmitter(QObject):
    response = Signal(dict)


class thumbnail_generate_qrunnable(QRunnable):
    def __init__(self, filepath='', size=QSize(40, 30)):
        super().__init__(filepath, size)
        self.filepath = filepath
        self.size = size
        self.emitter = MyEmitter()

    def run(thread):
        if thread.filepath:
            final_response_dict = {}
            final_response_dict[thread.filepath] = get_thumbnail(thread.size, filepath=thread.filepath, media_type='video')
            thread.emitter.response.emit(final_response_dict)


class GetProcAddressGetter:
    """ fixme: Class gets obsolete once https://bugreports.qt.io/browse/PYSIDE-971 gets fixed """

    def __init__(self):
        self._func = self._find_platform_wrapper()

    def _find_platform_wrapper(self):
        operating_system = platform.system()
        if operating_system == 'Linux':
            return self._init_linux()
        elif operating_system == 'Windows':
            return self._init_windows()
        raise f'Platform {operating_system} not supported yet'

    def _init_linux(self):
        try:
            from OpenGL import GLX
            return self._glx_impl
        except AttributeError:
            pass
        try:
            from OpenGL import EGL
            return self._egl_impl
        except AttributeError:
            pass
        raise 'Cannot initialize OpenGL'

    def _init_windows(self):
        import glfw
        from PySide6.QtGui import QOffscreenSurface, QOpenGLContext

        self.surface = QOffscreenSurface()
        self.surface.create()

        if not glfw.init():
            raise 'Cannot initialize OpenGL'

        glfw.window_hint(glfw.VISIBLE, glfw.FALSE)
        window = glfw.create_window(1, 1, "mpvQC-OpenGL", None, None)

        glfw.make_context_current(window)
        # QOpenGLContext.currentContext().makeCurrent(self.surface)
        return self._windows_impl

    def wrap(self, _, name: bytes):
        address = self._func(name)
        return ctypes.cast(address, ctypes.c_void_p).value

    @staticmethod
    def _glx_impl(name: bytes):
        from OpenGL import GLX
        return GLX.glXGetProcAddress(name.decode("utf-8"))

    @staticmethod
    def _egl_impl(name: bytes):
        from OpenGL import EGL
        return EGL.eglGetProcAddress(name.decode("utf-8"))

    @staticmethod
    def _windows_impl(name: bytes):
        import glfw
        return glfw.get_proc_address(name.decode('utf8'))


class MpvWidget(QOpenGLWidget):
    positionChanged = Signal(float, int)
    eofReached = Signal()

    def __init__(widget, parent=None):
        super().__init__(parent)

        widget.mpv = MPV(
            ytdl=False,
            loglevel='info',
            log_handler=print
        )

        widget.mpv_gl = None
        widget.get_proc_addr_c = MpvGlGetProcAddressFn(GetProcAddressGetter().wrap)
        widget.frameSwapped.connect(
            widget.swapped, Qt.ConnectionType.DirectConnection
        )

        for key, value in {
            # "config": False,
            'osd_level': 0,
            'sub_auto': False,
            'sub_ass': False,
            'sub_visibility': False,
            'keep_open': True,
            'cursor_autohide': False,
            'input_cursor': False,
            'input_default_bindings': False,
            'stop_playback_on_init_failure': False,
            'audio_file_auto': False,
            'input_vo_keyboard': False,
            'sid': False,
            "quiet": True,
            "msg-level": "all=info",
            "osc": False,
            "osd-bar": False,
            "input-cursor": False,
            "input-vo-keyboard": False,
            "input-default-bindings": False,
            "ytdl": False,
            "sub-auto": False,
            "audio-file-auto": False,
            "vo": "libmpv",
            "hwdec": "auto",
            "pause": True,
            "idle": True,
            "blend-subtitles": "video",
            "video-sync": "display-vdrop",
            "keepaspect": True,
            "stop-playback-on-init-failure": False,
            "keep-open": True,
            # "track-auto-selection": False,
        }.items():
            setattr(widget.mpv, key, value)

        widget.position = 0.0
        widget.mpv.observe_property('time-pos', widget.position_changed)
        # widget.mpv.observe_property('eof-reached', widget.eof_reached)

    def initializeGL(widget):
        widget.mpv_gl = MpvRenderContext(
            widget.mpv,
            api_type="opengl",
            opengl_init_params={"get_proc_address": widget.get_proc_addr_c},
        )
        widget.mpv_gl.update_cb = widget.on_update

    def paintGL(widget):
        if widget.mpv_gl:
            ratio = widget.devicePixelRatioF()
            w = int(widget.width() * ratio)
            h = int(widget.height() * ratio)
            fbo = widget.defaultFramebufferObject()
            widget.mpv_gl.render(
                flip_y=True,
                opengl_fbo={
                    "fbo": fbo,
                    "w": w,
                    "h": h,
                },
            )

    # @Slot()
    # def maybe_update(widget):
    #     """Maybeupdate function"""
    #     if widget.window().isMinimized():
    #         widget.makeCurrent()
    #         widget.paintGL()
    #         widget.context().swapBuffers(widget.context().surface())
    #         widget.swapped()
    #         widget.doneCurrent()
    #     else:
    #         widget.update()

    def on_update(widget, ctx=None):
        widget.update()
        # print(widget.width())
        # print(widget.height())

    def on_update_fake(widget, ctx=None):
        pass

    def swapped(widget):
        if widget.mpv_gl:
            widget.mpv_gl.report_swap()

    def closeEvent(widget, _):
        widget.makeCurrent()
        if widget.mpv_gl:
            widget.mpv_gl.update_cb = widget.on_update_fake
            widget.mpv_gl.free()

    def position_changed(widget, _, pos):
        """Position changed function. It calls update timeline paint."""
        if pos:
            widget.position = pos
        if pos is not None:
            widget.positionChanged.emit(pos, 1)
            # widget.parent.parent().timeline.update(widget.parent.parent())

    def eof_reached(widget, _, property):
        widget.eofReached.emit()

    def loadfile(widget, filepath) -> None:
        """Function to load a media file"""
        if os.path.isfile(filepath):
            widget.mpv.command('loadfile', filepath, 'replace')
            widget.mpv.wait_for_property('seekable')
        widget.mpv.pause = True

    def frameStep(widget) -> None:
        """Function to move forward one step (frame)"""
        widget.mpv.command('frame-step')

    def frameBackStep(widget) -> None:
        """Function to move backward one step (frame)"""
        widget.mpv.command('frame-back-step')

    def seek(widget, pos=0.0, method='absolute+exact') -> None:
        """Function to seek at some position"""
        widget.mpv.seek(pos, method)
        widget.position = pos

    def stop(widget) -> None:
        """Function to stop playback (fake stop, it is pause + position 0)"""
        widget.mpv.pause = True
        # print(dir(widget.mpv))
        if widget.mpv.filename:
            widget.position = 0.0
            widget.seek()

    def pause(widget) -> None:
        """Function to pause playback (fake pause, it just changes actual playback status)"""
        widget.mpv.pause = not widget.mpv.pause

    def play(widget) -> None:
        """Function to play (fake play, it just changes actual playback status)"""
        if widget.mpv.pause:
            widget.mpv.pause = False

    def mute(widget) -> None:
        """Function to mute"""
        widget.property('mute', not widget.property('mute'))

    def volume(widget, vol: int) -> None:
        """Function to change volume"""
        widget.property('volume', vol)

    # def resizeEvent(widget, event):
    #    event.accept()
    #    #print(widget.width())
    #    #print(widget.height())


def load(self):
    self.monitor_window_player = MpvWidget(parent=self.monitor_window)
    self.monitor_window_player.setVisible(False)
    self.monitor_window_player.setGeometry(0, 0, self.monitor_window.width(), self.monitor_window.height())
    self.monitor_window_player.positionChanged.connect(lambda: self.right_panel_monitor_player_timeline.update())
    # self.monitor_window_player.eofReached.connect(lambda: eof_reached_on_monitor(self))

    self.left_panel_widget_video_adddition_panel = QWidget()
    self.left_panel_widget_video_adddition_panel.setLayout(QVBoxLayout())

    self.left_panel_widget_video_addition_panel_tabwidget = QTabWidget()

    self.left_panel_widget_video_addition_panel_bookmarked_panel = QWidget()
    self.left_panel_widget_video_addition_panel_bookmarked_panel.setLayout(QVBoxLayout())
    self.left_panel_widget_video_addition_panel_bookmarked_panel.layout().setContentsMargins(0, 0, 0, 0)
    self.left_panel_widget_video_addition_panel_bookmarked_panel.layout().setSpacing(0)

    class left_panel_widget_video_addition_panel_bookmarked_qlistwidget(QListWidget):
        def __init__(widget, parent=None):
            super().__init__(parent)
            widget.list = []
            height_ratio = float(self.monitor_window.height()) / float(self.monitor_window.width())
            widget.icon_size = QSize(64, 64 * height_ratio)
            widget.setIconSize(widget.icon_size)

        def update(widget):
            widget.clear()
            for item in widget.list:
                icon = item.get('thumbnail', item.get('icon', QIcon(get_thumbnail(widget.icon_size, media_type='video'))))
                item_widget = QListWidgetItem(icon, item['title'])
                widget.addItem(item_widget)

        def add_item(widget, item):
            widget.list.append(item)
            save_bookmarked_files(self)
            widget.update()

        def remove_item(widget, item):
            del widget.list[item]
            save_bookmarked_files(self)
            widget.update()

        def leaveEvent(widget, event):
            self.left_panel_widget_video_addition_panel_buttons_line.setVisible(False)
            return event

        def enterEvent(widget, event):
            self.left_panel_widget_video_addition_panel_buttons_line.setVisible(True)
            return event

        def showEvent(widget, event):
            for item in widget.list:
                if not item.get('thumbnail', False):
                    worker = thumbnail_generate_qrunnable(filepath=item['filepath'], size=self.right_panel_preview.size())
                    worker.emitter.response.connect(lambda response: thumbnail_generate_qrunnable_response_received(self, response))
                    self.threadpool.start(worker)
            return event

    self.left_panel_widget_video_addition_panel_bookmarked_qlistwidget = left_panel_widget_video_addition_panel_bookmarked_qlistwidget()
    self.left_panel_widget_video_addition_panel_bookmarked_qlistwidget.setViewMode(QListView.IconMode)
    self.left_panel_widget_video_addition_panel_bookmarked_qlistwidget.currentItemChanged.connect(lambda: left_panel_widget_video_addition_panel_bookmarked_qlistwidget_currentItemChanged(self))
    self.left_panel_widget_video_addition_panel_bookmarked_qlistwidget.setLayout(QVBoxLayout())
    self.left_panel_widget_video_addition_panel_bookmarked_qlistwidget.layout().addStretch()

    self.left_panel_widget_video_addition_panel_buttons_line = QWidget()
    self.left_panel_widget_video_addition_panel_buttons_line.setLayout(QHBoxLayout())
    self.left_panel_widget_video_addition_panel_buttons_line.layout().setContentsMargins(0, 0, 0, 0)
    self.left_panel_widget_video_addition_panel_buttons_line.layout().addStretch()

    self.left_panel_widget_video_addition_panel_add_button = QPushButton()
    self.left_panel_widget_video_addition_panel_add_button.setObjectName('left_panel_widget_video_addition_panel_add_button')
    self.left_panel_widget_video_addition_panel_add_button.clicked.connect(lambda: left_panel_widget_video_addition_panel_add_button_clicked(self))
    self.left_panel_widget_video_addition_panel_buttons_line.layout().addWidget(self.left_panel_widget_video_addition_panel_add_button)

    self.left_panel_widget_video_addition_panel_remove_button = QPushButton()
    self.left_panel_widget_video_addition_panel_remove_button.setObjectName('left_panel_widget_video_addition_panel_remove_button')
    self.left_panel_widget_video_addition_panel_remove_button.setVisible(False)
    self.left_panel_widget_video_addition_panel_remove_button.clicked.connect(lambda: left_panel_widget_video_addition_panel_remove_button_clicked(self))
    self.left_panel_widget_video_addition_panel_buttons_line.layout().addWidget(self.left_panel_widget_video_addition_panel_remove_button)

    self.left_panel_widget_video_addition_panel_bookmarked_qlistwidget.layout().addWidget(self.left_panel_widget_video_addition_panel_buttons_line, 0)

    self.left_panel_widget_video_addition_panel_bookmarked_panel.layout().addWidget(self.left_panel_widget_video_addition_panel_bookmarked_qlistwidget)

    self.left_panel_widget_video_addition_panel_tabwidget.addTab(self.left_panel_widget_video_addition_panel_bookmarked_panel, '')

    self.left_panel_widget_video_adddition_panel.layout().addWidget(self.left_panel_widget_video_addition_panel_tabwidget)

    self.left_panel_widget.stacked_widget.addWidget(self.left_panel_widget_video_adddition_panel)

    self.left_panel_widget_video_addition_menu_button = left_panel_addition_menu_button()
    self.left_panel_widget_video_addition_menu_button.setObjectName('left_panel_widget_video_addition_menu_button')
    self.left_panel_widget_video_addition_menu_button.setProperty('type', 'video')
    self.left_panel_widget_video_addition_menu_button.clicked.connect(lambda: addition_selected(self))
    self.left_panel_widget.vertical_menu.layout().addWidget(self.left_panel_widget_video_addition_menu_button)

    # self.thumbnail_generate_qrunnable = thumbnail_generate_qrunnable()
    # self.thumbnail_generate_qrunnable.response.connect(thumbnail_generate_qrunnable_response_received)
    load_bookmarked_files(self)

    self.right_panel_monitor_player = QWidget()
    self.right_panel_monitor_player.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)
    self.right_panel_monitor_player.setLayout(QHBoxLayout())
    self.right_panel_monitor_player.layout().setContentsMargins(0, 0, 0, 0)
    self.right_panel_monitor_player.layout().setSpacing(0)
    self.right_panel_monitor_player.setVisible(False)

    self.right_panel_monitor_player_stopbutton = QPushButton()
    self.right_panel_monitor_player_stopbutton.setObjectName('right_panel_monitor_player_stopbutton')
    self.right_panel_monitor_player_stopbutton.setSizePolicy(QSizePolicy.Maximum, QSizePolicy.Maximum)
    # self.right_panel_monitor_player_stopbutton.setStyleSheet('QPushButton { image: url(' + os.path.join(PATH_PRESENTD_GRAPHICS, 'stop_icon.png').replace('\\', '/') + '); border-top: 5px; border-right: 0px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_dark_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 0px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_dark_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 0px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_dark_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_dark_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
    self.right_panel_monitor_player_stopbutton.clicked.connect(lambda: right_panel_monitor_player_stopbutton_pressed(self))
    self.right_panel_monitor_player.layout().addWidget(self.right_panel_monitor_player_stopbutton)

    self.right_panel_monitor_player_playbutton = QPushButton()
    self.right_panel_monitor_player_playbutton.setObjectName('right_panel_monitor_player_playbutton')
    self.right_panel_monitor_player_playbutton.setCheckable(True)
    self.right_panel_monitor_player_playbutton.setSizePolicy(QSizePolicy.Maximum, QSizePolicy.Maximum)
    # self.right_panel_monitor_player_playbutton.setStyleSheet('QPushButton { image: url(' + os.path.join(PATH_PRESENTD_GRAPHICS, 'play_icon.png').replace('\\', '/') + ');  border-top: 5px; border-right: 0px; border-bottom: 5px; border-left: 0px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_dark_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 0px; border-bottom: 5px; border-left: 0px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_dark_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 0px; border-bottom: 5px; border-left: 0px; border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_dark_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { image: url(' + os.path.join(PATH_PRESENTD_GRAPHICS, 'pause_icon.png').replace('\\', '/') + '); border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_dark_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:checked { image: url(' + os.path.join(PATH_PRESENTD_GRAPHICS, 'pause_icon.png').replace('\\', '/') + '); border-image: url("' + os.path.join(PATH_PRESENTD_GRAPHICS, "button_dark_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
    self.right_panel_monitor_player_playbutton.clicked.connect(lambda: right_panel_monitor_player_playpause_pressed(self))
    self.right_panel_monitor_player.layout().addWidget(self.right_panel_monitor_player_playbutton)

    # self.right_panel_monitor_player_timelinebox = QLabel()

    class right_panel_monitor_player_timeline(QLabel):
        def __init__(widget, parent=None):
            super().__init__(parent)
            widget.setObjectName('right_panel_monitor_player_timeline')
            widget.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)

        def paintEvent(widget, paintEvent):
            painter = QPainter(widget)
            if widget.isVisible():  # and self.monitor_window_player.position > 0:
                # painter.setBrush(QColor.fromRgb(24, 33, 41))
                # painter.drawRect(0, 0, widget.width(), widget.height())
                widget_area = QRectF(0, 0, widget.width(), widget.height()) - QMargins(0, 2, 2, 2)

                painter.setRenderHint(QPainter.Antialiasing)

                painter.setPen(QColor.fromRgb(102, 102, 102))
                painter.setFont(QFont("Ubuntu", 7))
                rectangle = widget_area - QMargins(2, 0, widget_area.width() * .5, widget_area.height() * .5)

                total_time = str(convert_to_timecode(self.monitor_window_player.mpv.duration, True))
                painter.drawText(rectangle, Qt.AlignVCenter | Qt.AlignLeft, total_time)
                time_to_stop = self.monitor_window_player.mpv.duration - self.monitor_window_player.position

                if (int(time_to_stop) < 31 and int(time_to_stop) % 2 == 0) or int(time_to_stop) < 10:
                    painter.setPen(QColor.fromRgb(255, 0, 0))
                else:
                    painter.setPen(QColor.fromRgb(102, 102, 102))
                rectangle = widget_area - QMargins(widget_area.width() * .5, 0, 2, widget_area.height() * .5)
                if self.monitor_window_player.position > 0:
                    painter.drawText(rectangle, Qt.AlignVCenter | Qt.AlignRight, str(convert_to_timecode(time_to_stop, True)))

                painter.setPen(Qt.NoPen)
                painter.setBrush(QColor.fromRgb(24, 33, 41))
                rectangle = widget_area - QMargins(0, widget_area.height() * .5, 0, 0)
                painter.drawRect(rectangle)

                painter.setBrush(QColor.fromRgb(48, 66, 81))

                rectangle = widget_area - QMargins(0, widget_area.height() * .5, widget_area.width() - (widget_area.width() * (self.monitor_window_player.position / self.monitor_window_player.mpv.duration)), 0)
                painter.drawRect(rectangle)
                # rectangle = QRectF(0, widget.height() * .5, widget.width() * (self.monitor_window_player.position / self.monitor_window_player.mpv.duration), widget.height() * .5)

                painter.setPen(QColor.fromRgb(255, 255, 255))
                rectangle = rectangle - QMargins(0, 0, 2, 0)
                # rectangle = QRectF(0, widget.height() * .5, (widget.width() * (self.monitor_window_player.position / self.monitor_window_player.mpv.duration)) - 3, widget.height() * .5)
                painter.drawText(rectangle, Qt.AlignVCenter | Qt.AlignRight, str(convert_to_timecode(self.monitor_window_player.position, True)))

            painter.end()
            return paintEvent

    self.right_panel_monitor_player_timeline = right_panel_monitor_player_timeline()
    self.right_panel_monitor_player.layout().addWidget(self.right_panel_monitor_player_timeline)

    self.right_panel_properties.layout().addWidget(self.right_panel_monitor_player)


def show_media(self, addition):
    self.right_panel_monitor_player.setVisible(True)
    self.monitor_window_player.setVisible(True)
    self.monitor_window_player.loadfile(addition['media'])
    self.monitor_window_player.play()
    self.right_panel_monitor_player_playbutton.setChecked(True)


def hide_media(self):
    self.monitor_window_player.setVisible(False)
    self.monitor_window_player.stop()
    self.right_panel_monitor_player.setVisible(False)
    self.right_panel_monitor_player_playbutton.setChecked(False)


def eof_reached_on_monitor(self):
    self.monitor_window_player.setVisible(False)
    self.right_panel_monitor_player.setVisible(False)
    self.right_panel_monitor_player_playbutton.setChecked(False)


def translate(self):
    self.left_panel_widget_video_addition_panel_tabwidget.setTabText(0, _('modules.video.bookmarked'))
    # self.left_panel_widget_video_addition_panel_add_button.setText(_('modules.video.add_video'))
    # self.left_panel_widget_video_addition_panel_remove_button.setText(_('modules.video.remove_video'))


def left_panel_widget_video_addition_panel_remove_button_clicked(self):
    if self.left_panel_widget_video_addition_panel_bookmarked_qlistwidget.currentItem():
        self.left_panel_widget_video_addition_panel_bookmarked_qlistwidget.remove_item(self.left_panel_widget_video_addition_panel_bookmarked_qlistwidget.currentRow())


def left_panel_widget_video_addition_panel_add_button_clicked(self):
    video_path_list = QFileDialog.getOpenFileNames(self, 'Select the media files to add', REAL_PATH_HOME, "Video files (*.m4v *.mp4 *.M4V *.MP4)")[0]
    for filename in video_path_list:
        if os.path.isfile(filename):
            item = {
                'title': os.path.basename(filename),
                'filepath': filename,
                'icon': get_thumbnail(self.left_panel_widget_video_addition_panel_bookmarked_qlistwidget.icon_size, media_type='video')
            }
            self.left_panel_widget_video_addition_panel_bookmarked_qlistwidget.add_item(item)

            worker = thumbnail_generate_qrunnable(filepath=filename, size=self.right_panel_preview.size())
            worker.emitter.response.connect(lambda response: thumbnail_generate_qrunnable_response_received(self, response))
            self.threadpool.start(worker)


def left_panel_widget_video_addition_panel_bookmarked_qlistwidget_currentItemChanged(self):
    if self.left_panel_widget_video_addition_panel_bookmarked_qlistwidget.currentItem():
        self.left_panel_addbutton.show()
    self.left_panel_widget_video_addition_panel_remove_button.setVisible(bool(self.left_panel_widget_video_addition_panel_bookmarked_qlistwidget.currentItem()))


def addition_selected(self):
    addition = {
        'type': 'video',
        'show_widgets_function': lambda: show_addition_widgets(self),
        'add_to_playlist_function': lambda: add_video_to_playlist(self),
    }
    self.left_panel_widget.addition_clicked(addition)


def show_addition_widgets(self):
    self.left_panel_widget.stacked_widget.setCurrentWidget(self.left_panel_widget_video_adddition_panel)


def add_video_to_playlist(self):
    media = self.left_panel_widget_video_addition_panel_bookmarked_qlistwidget.list[self.left_panel_widget_video_addition_panel_bookmarked_qlistwidget.currentRow()]
    # video_mp4_piece = MP4(media['filepath'])

    title = media.get('title', '')
    # if '\xa9nam' in  [*video_mp4_piece.tags]:
    #     title = video_mp4_piece.tags['\xa9nam'][0]
    # if '\xa9alb' in  [*video_mp4_piece.tags]:
    #     album = video_mp4_piece.tags['\xa9alb'][0]

    addition = {
        'type': 'video',
        'media': media.get('filepath', ''),
        'title': title,
        'hide_media_function': lambda: hide_media(self),
        'thumbnail': media.get('thumbnail', ''),
        # 'duration': video_mp4_piece.info.length
    }
    addition['show_media_function'] = lambda: show_media(self, addition)

    playlist_panel.add_media_to_playlist(self, addition)


def load_bookmarked_files(self):
    self.left_panel_widget_video_addition_panel_bookmarked_qlistwidget.list = []
    # pool = QThreadPool.globalInstance()
    # filepath_list = []
    # print(self.settings.get('video_bookmarked_files', []))
    for filepath in self.settings.get('video_bookmarked_files', []):
        if os.path.isfile(filepath):
            item = {
                'title': os.path.basename(filepath),
                'filepath': filepath,
                'icon': get_thumbnail(self.left_panel_widget_video_addition_panel_bookmarked_qlistwidget.icon_size, media_type='video')
            }
            self.left_panel_widget_video_addition_panel_bookmarked_qlistwidget.add_item(item)

            # worker = thumbnail_generate_qrunnable(filepath=filepath, size=self.right_panel_preview.size())
            # worker.emitter.response.connect(lambda response: thumbnail_generate_qrunnable_response_received(self, response))
            # self.threadpool.start(worker)


def save_bookmarked_files(self):
    self.settings['video_bookmarked_files'] = [item.get('filepath', '') for item in self.left_panel_widget_video_addition_panel_bookmarked_qlistwidget.list]


def right_panel_monitor_player_stopbutton_pressed(self):
    self.monitor_window_player.stop()
    self.right_panel_monitor_player_playbutton.setChecked(False)


def right_panel_monitor_player_playpause_pressed(self):
    self.monitor_window_player.pause()


def thumbnail_generate_qrunnable_response_received(self, response):
    for item in self.left_panel_widget_video_addition_panel_bookmarked_qlistwidget.list:
        for video_file in response:
            if item.get('filepath', False) and item['filepath'] == video_file:
                item['thumbnail'] = response[video_file]
                # item['icon'] = QIcon(item['thumbnail'])
    self.left_panel_widget_video_addition_panel_bookmarked_qlistwidget.update()
