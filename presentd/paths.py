"""All path definitions for Subtitld"""

import os
import sys
import tempfile
import random
import subprocess
import presentd

PATH_PRESENTD = os.path.dirname(presentd.__file__)

PATH_HOME = os.path.expanduser("~")
REAL_PATH_HOME = PATH_HOME

FFMPEG_EXECUTABLE = 'ffmpeg'
FFPROBE_EXECUTABLE = 'ffprobe'

STARTUPINFO = None

ACTUAL_OS = 'linux'

PATH_TMP = os.path.join(tempfile.gettempdir(), 'presentd-' + str(random.randint(1000, 9999)))
if sys.platform == 'darwin':
    ACTUAL_OS = 'macos'
    PATH_PRESENTD_USER_CONFIG = os.path.join(PATH_HOME, 'Library', 'Application Support', 'presentd')
    FFMPEG_EXECUTABLE = os.path.join(os.path.abspath(os.path.dirname(sys.argv[0])), 'ffmpeg')
    FFPROBE_EXECUTABLE = os.path.join(os.path.abspath(os.path.dirname(sys.argv[0])), 'ffprobe')
    # try:
    #     from Foundation import NSURL
    # except ImportError:
    #     sys.path.append('/System/Library/Frameworks/Python.framework/Versions/2.7/Extras/lib/python/PyObjC')
    #     from Foundation import NSURL
elif sys.platform == 'win32' or os.name == 'nt':
    ACTUAL_OS = 'windows'
    PATH_PRESENTD = os.path.abspath(os.path.dirname(sys.argv[0]))
    PATH_PRESENTD_CACHE = os.path.join(os.getenv('LOCALAPPDATA'), 'presentd')
    PATH_PRESENTD_USER_CONFIG = os.path.join(os.getenv('APPDATA'), 'presentd')
    FFMPEG_EXECUTABLE = os.path.join(os.path.abspath(os.path.dirname(sys.argv[0])), 'ffmpeg.exe')
    FFPROBE_EXECUTABLE = os.path.join(os.path.abspath(os.path.dirname(sys.argv[0])), 'ffprobe.exe')
    STARTUPINFO = subprocess.STARTUPINFO()
    STARTUPINFO.dwFlags |= subprocess.STARTF_USESHOWWINDOW
    STARTUPINFO.wShowWindow = subprocess.SW_HIDE
else:
    try:
        REAL_PATH_HOME = subprocess.Popen(['getente', 'passwd', str(os.getuid())], stdout=subprocess.PIPE).stdout.read().decode().split(':')[5]
    except FileNotFoundError:
        pass

    if not os.path.isdir(os.path.join(PATH_HOME, '.config')):
        os.mkdir(os.path.join(PATH_HOME, '.config'))
    PATH_PRESENTD_USER_CONFIG = os.path.join(PATH_HOME, '.config', 'presentd')

    if not os.path.isdir(os.path.join(PATH_HOME, '.cache')):
        os.mkdir(os.path.join(PATH_HOME, '.cache'))
    PATH_PRESENTD_CACHE = os.path.join(PATH_HOME, '.cache', 'presentd')

PATH_PRESENTD_GRAPHICS = os.path.join(PATH_PRESENTD, 'graphics')
PATH_LOCALE = os.path.join(PATH_PRESENTD, 'locale')

os.mkdir(PATH_TMP)

if not os.path.isdir(PATH_PRESENTD_USER_CONFIG):
    os.mkdir(PATH_PRESENTD_USER_CONFIG)

if not os.path.isdir(PATH_PRESENTD_CACHE):
    os.mkdir(PATH_PRESENTD_CACHE)

PATH_PRESENTD_USER_CONFIG_FILE = os.path.join(PATH_PRESENTD_USER_CONFIG, 'presentd.config')


def get_graphics_path(filename):
    """Function to get graphics path. Windows have a problem with paths."""
    final_path = os.path.join(PATH_PRESENTD_GRAPHICS, filename)
    if sys.platform == 'win32' or os.name == 'nt':
        final_path = final_path.replace('\\', '/')
    return final_path

VERSION_NUMBER = '20.07.0.0'
if os.path.isfile(os.path.join(PATH_PRESENTD, 'current_version')):
    VERSION_NUMBER = open(os.path.join(PATH_PRESENTD, 'current_version')).read().strip()
