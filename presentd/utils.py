from mutagen.mp4 import MP4
import subprocess
import os
import xxhash

from PySide6.QtWidgets import QLabel
from PySide6.QtGui import QPixmap
from PySide6.QtCore import QRect, Qt

from presentd.paths import PATH_PRESENTD_CACHE, PATH_TMP, get_graphics_path, FFMPEG_EXECUTABLE


def generate_effect(animation, effect_type, duration, startValue, endValue):
    animation.setDuration(duration)
    if effect_type == 'geometry':
        if not startValue:
            startValue = QRect(animation.targetObject().rect())
        else:
            startValue = QRect(int(startValue[0]), int(startValue[1]), int(startValue[2]), int(startValue[3]))
        animation.setStartValue(startValue)
        animation.setEndValue(QRect(int(endValue[0]), int(endValue[1]), int(endValue[2]), int(endValue[3])))
    elif effect_type == 'opacity':
        animation.setStartValue(startValue)
        animation.setEndValue(endValue)
    animation.start()


def convert_to_timecode(value, simple=False):
    fr = int(str('%.3f'% value).split('.')[-1])
    mm, ss = divmod(value, 60)
    hh, mm = divmod(mm, 60)

    if simple:
        if hh == 0 and mm == 0:
            return '%02d' % (ss)
        elif hh == 0:
            return '%02d:%02d' % (mm, ss)
        else:
            return '%02d:%02d:%02d' % (hh, mm, ss)
    else:
        return '%02d:%02d:%02d.%03d' % (hh, mm, ss, fr)


def get_thumbnail(size, filepath='', media_type='media'):
    thumbnail = QLabel()
    thumbnail.resize(size)
    thumbnail.setAlignment(Qt.AlignCenter)

    thumb_tmp_file = ''
    if filepath:
        digest = ''
        with open(filepath, 'rb') as afile:
            digest = str(xxhash.xxh32(afile.read()).hexdigest())

        if digest:
            thumb_tmp_file = os.path.join(PATH_PRESENTD_CACHE, '{}.png'.format(digest))

            if not os.path.isfile(os.path.join(PATH_PRESENTD_CACHE, '{}.png'.format(digest))):
                if media_type == 'video':
                    video = MP4(filepath)
                    if video.tags and 'covr' in [*video.tags]:
                        artwork = video.tags["covr"][0]
                        open(os.path.join(PATH_TMP, 'thumbnail.png'), 'wb').write(artwork)
                    else:
                        subprocess.call(
                            [
                                FFMPEG_EXECUTABLE,
                                '-loglevel', 'error',
                                '-y',
                                '-ss', '00:05.00',
                                '-i', filepath,
                                '-frames:v', '1',
                                thumb_tmp_file
                            ]
                        )
                elif media_type == 'image':
                    QPixmap(filepath).save(thumb_tmp_file)

        if thumb_tmp_file:
            thumbnail.setStyleSheet('background-color: black;')
            thumbnail.setPixmap(QPixmap(thumb_tmp_file).scaled(size, Qt.KeepAspectRatio, Qt.SmoothTransformation))

        if not filepath or not thumb_tmp_file:
            thumbnail.setStyleSheet('background: qlineargradient(x1: 0, y1: 0, x2: 1, y2: 1, stop: 0 #f0f0f0, stop: 1 #e0e0e0);')
            if media_type == 'video':
                icon_image_file = get_graphics_path('video_icon_normal.png')
            elif media_type == 'image':
                icon_image_file = get_graphics_path('image_icon_normal.png')

            thumbnail.setScaledContents(False)
            thumbnail.setPixmap(QPixmap(icon_image_file))

    return thumbnail.grab()