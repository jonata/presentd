# -*- mode: python -*-

VERSION = '20.07.0.0'
if 'VERSION_NUMBER' in [*os.environ] and not os.environ['VERSION_NUMBER'] == '':
    VERSION = os.environ['VERSION_NUMBER']

a = Analysis(['presentd/__main__.py'],
    pathex=['/Users/admin/Documents/presentd'],
    binaries=[
        ( 'resources/mpv-1.dll', '.')
    ],
    datas=[
        ( 'presentd/graphics/*.png', 'graphics' ),
        ( 'presentd/graphics/*.svg', 'graphics' ),
        ( 'presentd/graphics/*.ttf', 'graphics' ),
        ( 'presentd/graphics/*.qss', 'graphics' ),
        ( 'presentd/locale/*.json', 'locale' ),
        ( 'ffmpeg-*/bin/ffmpeg.exe', '.'),
        ( 'ffmpeg-*/bin/ffprobe.exe', '.'),
        ( 'c:/python37/lib/site-packages/glfw/glfw3.dll', '.'),
    ],
    hiddenimports=[],
    hookspath=[],
    runtime_hooks=[] )

pyz = PYZ(a.pure)

exe = EXE(pyz,
    a.scripts,
    exclude_binaries=True,
    name='Presentd.exe',
    strip=False,
    upx=True,
    console=False,
    debug=False,
    icon='presentd/graphics/presentd.ico' )

coll = COLLECT( exe,
                a.binaries,
                a.zipfiles,
                a.datas,
                strip=False,
                upx=True,
                name='Presentd')

open('dist/presentd/current_version', mode='w', encoding='utf-8').write(VERSION)
