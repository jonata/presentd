# -*- coding: utf-8 -*-

#######################################################################
#
# Presentd
#
#######################################################################

from setuptools import setup, find_packages

from helpers import SetupHelpers
import presentd

# --------------------------------------------------------------------------- #

setup_requires = ['setuptools']
install_requires = [
    'PySide6==6.2.4',
    'pyopengl',
    'mutagen',
    'python-mpv==0.5.2',
    'python-i18n',
    'xxhash'
]

# --------------------------------------------------------------------------- #

try:
    # begin setuptools installer
    result = setup(
        app=['presentd/__main__.py'],
        name=presentd.__appname__.lower(),
        version=presentd.__version__,
        author=presentd.__author__,
        author_email=presentd.__email__,
        description='Presentd',
        long_description=SetupHelpers.get_description(),
        url=presentd.__website__,
        license='GPL3',
        packages=find_packages(include=['presentd', 'presentd.*']),
        setup_requires=setup_requires,
        install_requires=install_requires,
        data_files=SetupHelpers.get_data_files(),
        package_data=SetupHelpers.get_package_files(),
        include_package_data=True,
        entry_points={'gui_scripts': ['presentd = presentd.__main__:main']},
        keywords='presentd',
        classifiers=[
            'Development Status :: 5 - Production/Stable',
            'Environment :: X11 Applications :: Qt',
            'Intended Audience :: End Users/Desktop',
            'License :: GPL3',
            'Natural Language :: English',
            'Operating System :: POSIX',
            'Topic :: Office',
            'Programming Language :: Python :: 3 :: Only'
        ],
        options={'py2app': {
            'argv_emulation': True,
            # 'iconfile': 'src/Icon.icns',  # optional
            # 'plist': 'src/Info.plist',    # optional
        }},
    )
except BaseException:
    if presentd.__ispypi__:
        SetupHelpers.pip_notes()
    raise
